<?php


namespace App\Admin\Widget\FilterNav;


use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Widget;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

class Nav extends Widget
{
    protected $view = 'admin.widget.filter-nav.nav';

    protected $url;

    protected $field;

    protected $values = [];

    public function __construct(string $field, array $values, string $url = null)
    {
        parent::__construct([]);

        $url = $url ?? Route::current()->uri();
        $this->url = $url;
        $this->field = $field;
        $this->values = $values;
    }

    /*public function addLink($href, $text = '')
    {
        $this->links = []
    }*/

    protected function links()
    {
        $default = new Link('Все', url($this->url));
        if (Request::query($this->field) === null) {
            $default->setActive(true);
        }

        $links = [$default];
        foreach ($this->values as $key => $value) {
            $link = new Link($value);
            $url = url( $this->url . '?' . $this->field . '=' . $key);
            $link->setHref($url);
            if (Request::query($this->field) !== null && Request::query($this->field) == $key) {
                $link->setActive(true);
            }
            $links[] = $link;
        }
        return $links;
    }

    public function render()
    {
        return view($this->view, [
            'links' => $this->links()
        ])->render();
    }
}
