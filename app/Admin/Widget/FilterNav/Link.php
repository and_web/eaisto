<?php


namespace App\Admin\Widget\FilterNav;


use Illuminate\Contracts\Support\Renderable;

class Link
{
    protected $href;

    protected $text;

    protected $active;

    public function __construct(string $text, string $href = '#')
    {
        $this->text = $text;
        $this->href = $href;
    }

    public function setHref(string $href)
    {
        $this->href = $href;
    }

    public function setActive(bool $active = false)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
        //return url()->full() === $this->href;
    }

}
