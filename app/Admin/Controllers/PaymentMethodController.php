<?php

namespace App\Admin\Controllers;

use App\Models\PaymentMethod;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PaymentMethodController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'PaymentMethod';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PaymentMethod());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('label', __('Label'));
        $grid->column('value', __('Value'));
        $grid->column('logo', __('Logo'))->image();
        $grid->column('description', __('Description'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PaymentMethod::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('label', __('Label'));
        $show->field('value', __('Value'));
        $show->field('logo', __('Logo'))->image();
        $show->field('description', __('Description'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PaymentMethod());

        $form->text('name', __('Name'));
        $form->text('label', __('Label'));
        $form->text('value', __('Value'));
        $form->text('logo', __('Logo'));
        $form->textarea('description', __('Description'));

        return $form;
    }
}
