<?php

namespace App\Admin\Controllers;

use App\Models\Region;
use App\Models\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Агенты';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());

        $grid->column('id', __('Id'));
        $grid->column('email', __('Email'));
        $grid->column('last_name', __('Last name'));
        $grid->column('first_name', __('First name'));
        $grid->column('middle_name', __('Middle name'));
        $grid->column('region.name', __('Region'));
        $grid->column('city', __('City'));
        $grid->column('phone', __('Phone'));
        $grid->column('need', __('Need'));
        //$grid->column('status', __('Status'));
        //$grid->column('insurance_company', __('Insurance company'));


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('email', __('Email'));
        $show->field('email_verified_at', __('Email verified at'));
        $show->field('password', __('Password'));
        $show->field('remember_token', __('Remember token'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('first_name', __('First name'));
        $show->field('last_name', __('Last name'));
        $show->field('middle_name', __('Middle name'));
        $show->field('region.name', __('Region'));
        $show->field('city', __('City'));
        $show->field('need', __('Need'));
        $show->field('status', __('Status'));
        $show->field('insurance_company', __('Insurance company'));
        $show->field('phone', __('Phone'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User());

        $form->email('email', __('Email'));
        $form->datetime('email_verified_at', __('Email verified at'))->default(date('Y-m-d H:i:s'));
        $form->password('password', __('Password'));
        $form->text('remember_token', __('Remember token'));
        $form->text('first_name', __('First name'));
        $form->text('last_name', __('Last name'));
        $form->text('middle_name', __('Middle name'));
        $form->select('region_id', __('Region'))->options(
            Region::all()->pluck('name', 'id')
        );
        $form->text('city', __('City'));
        $form->number('need', __('Need'));
        $form->switch('status', __('Status'));
        $form->text('insurance_company', __('Insurance company'));
        $form->mobile('phone', __('Phone'));

        return $form;
    }
}
