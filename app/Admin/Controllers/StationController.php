<?php

namespace App\Admin\Controllers;

use App\Models\Region;
use App\Models\Station;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Grid\Filter;
use Encore\Admin\Show;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class StationController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Station';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Station());

        $grid->filter(function(Filter $filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            //$filter->expand();

            $filter->where(function (Builder $query) {
                if ($this->input) {
                    $query->join('regions', 'id')->where('region_id', $this->input);
                }
            }, 'Регион', 'region')->select(
                Region::query()->select(['id', 'name'])->get()->mapWithKeys(function ($item) {
                    return [$item['id'] => $item['name']];
                }),
            );
        });

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('rsa_id', 'Номер RSA');
        //$grid->column('oto', __('Oto'));
        //$grid->column('pto', __('Pto'));
        $grid->column('expert_id', __('Expert id'));
        $grid->column('expert_name', __('Expert name'));
        //$grid->column('start_id', __('Start id'));
        //$grid->column('certificate', __('Certificate'));
        $grid->column('owner', __('Owner'));
        //$grid->column('valid_from', __('Valid from'));
        //$grid->column('valid_to', __('Valid to'));
        $grid->column('region_id', __('Region id'))->display(function () {
            return $this->region->name;
        })->sortable();
        //$grid->column('created_at', __('Created at'));
        //$grid->column('updated_at', __('Updated at'));
        $grid->column('status', __('Is active'))->display(function(){
           return $this->status ? __('Yes') : __('No');
        })->label([
            0 => 'danger',
            2 => 'success',
        ]);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Station::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('oto', __('Oto'));
        $show->field('pto', __('Pto'));
        $show->field('expert_id', __('Expert id'));
        $show->field('expert_name', __('Expert name'));
        $show->field('start_id', __('Start id'));
        $show->field('certificate', __('Certificate'));
        $show->field('owner', __('Owner'));
        $show->field('valid_from', __('Valid from'));
        $show->field('valid_to', __('Valid to'));
        $show->field('region_id', __('Region id'));
        $show->field('status', 'Status')->label();
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Station());

        $form->textarea('oto', __('Oto'));
        $form->textarea('pto', __('Pto'));
        $form->number('expert_id', __('Expert id'));
        $form->text('expert_name', __('Expert name'));
        $form->number('start_id', __('Start id'));
        $form->text('certificate', __('Certificate'));
        $form->text('owner', __('Owner'));
        $form->datetime('valid_from', __('Valid from'))->default(date('Y-m-d H:i:s'));
        $form->datetime('valid_to', __('Valid to'))->default(date('Y-m-d H:i:s'));
        $form->number('region_id', __('Region id'));
        $form->switch('status', __('Is active'))->states([
            'on'  => ['value' => 1, 'text' => __('Yes'), 'color' => 'success'],
            'off' => ['value' => 0, 'text' => __('No'), 'color' => 'danger'],
        ]);

        return $form;
    }
}
