<?php

namespace App\Admin\Controllers;

use App\Models\BrakeType;
use App\Models\DocumentType;
use App\Models\FuelType;
use App\Models\LicenseCategory;
use App\Models\Order;
use App\Models\TechnicalCategory;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class OrderController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Order';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Order());

        $grid->model()->with('licenseCategory')->where([
            ['status', '!=', Order::STATUS_DRAFT],
        ]);

        $grid->column('id', __('Id'));
        $grid->column('user_id', __(''))->display(function () {
            /** @var Order $this */
            return '<a href="' . route('admin.users.show', ['user' => $this->user_id]) . '"> '
                . $this->user_id . '</a>';
        });
        $grid->column('user.region.name', 'Регион');
//        $grid->column('vehicle', 'ТС')->display(function () {
//            /** @var Order $this */
//            return $this->brand . ' ' . $this->model . ' ' . $this->licenseCategory->name . ' ' . $this->numberplate;
//        });
        $grid->column('created_at', __('Дата'));
        $grid->column('brand', 'Марка');
        $grid->column('model', 'Модель');
        $grid->column('licenseCategory.name', 'Категория');
        $grid->column('numberplate', 'Номер');
        $grid->column('status', 'Статус')->display(function () {
            /** @var Order $this */
            return trans(ucfirst($this->status));
        });
        $grid->column('price', 'Стоимость');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Order::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', __('User id'));
        $show->field('first_name', __('First name'));
        $show->field('middle_name', __('Middle name'));
        $show->field('last_name', __('Last name'));
        $show->field('phone', __('Phone'));
        $show->field('company', __('Company'));
        $show->field('vin', __('Vin'));
        $show->field('year', __('Year'));
        $show->field('brand', __('Brand'));
        $show->field('model', __('Model'));
        $show->field('body', __('Body'));
        $show->field('chassis', __('Chassis'));
        $show->field('mileage', __('Mileage'));
        $show->field('power', __('Power'));
        $show->field('numberplate', __('Numberplate'));
        $show->field('licenseCategory.name', __('License category'));
        $show->field('technicalCategory.name', __('Technical category'));
        $show->field('tires', __('Tires'));
        $show->field('net_weight', __('Net weight'));
        $show->field('max_weight', __('Max weight'));
        $show->field('fuelType.name', __('Fuel type'));
        $show->field('brakeType.name', __('Brake type'));
        $show->field('documentType.name', __('Document type'));
        $show->field('document_series', __('Document series'));
        $show->field('document_number', __('Document number'));
        $show->field('document_issue_date', __('Document issue date'));
        $show->field('document_issue_by', __('Document issue by'));
        $show->field('tachograph_brand', __('Tachograph brand'));
        $show->field('tachograph_model', __('Tachograph model'));
        $show->field('tachograph_series', __('Tachograph series'));
        $show->field('gas_equipment_number', __('Gas equipment number'));
        $show->field('gas_equipment_next_survey', __('Gas equipment next survey'));
        $show->field('gas_cylinder_serial_number', __('Gas cylinder serial number'));
        $show->field('gas_cylinder_date_last_survey', __('Gas cylinder date last survey'));
        $show->field('gas_cylinder_date_next_survey', __('Gas cylinder date next survey'));
        $show->field('gas_cylinder_year_issue', __('Gas cylinder year issue'));
        $show->field('car_type', __('Car type'));
        $show->field('status', __('Status'));
        $show->field('price', __('Price'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Order());

        $form->number('user_id', __('User id'))->disable();
        $form->text('first_name', __('First name'));
        $form->text('middle_name', __('Middle name'));
        $form->text('last_name', __('Last name'));
        $form->mobile('phone', __('Phone'));
        $form->text('company', __('Company'));
        $form->text('vin', __('Vin'));
        $form->text('year', __('Year'));
        $form->text('brand', __('Brand'));
        $form->text('model', __('Model'));
        $form->text('body', __('Body'));
        $form->text('chassis', __('Chassis'));
        $form->number('mileage', __('Mileage'));
        $form->number('power', __('Power'));
        $form->text('numberplate', __('Numberplate'));
        $form->select('license_category_id', __('License category'))->options(
            LicenseCategory::all()->pluck('name', 'id')
        );
        $form->select('technical_category_id', __('Technical category'))->options(
            TechnicalCategory::all()->pluck('name', 'id')
        );
        $form->text('tires', __('Tires'));
        $form->number('net_weight', __('Net weight'));
        $form->number('max_weight', __('Max weight'));
        $form->select('fuel_type_id', __('Fuel type'))->options(
            FuelType::all()->pluck('name', 'id')
        );
        $form->select('brake_type_id', __('Brake type'))->options(
            BrakeType::all()->pluck('name', 'id')
        );
        $form->select('document_type_id', __('Document type'))->options(
            DocumentType::all()->pluck('name', 'id')
        );
        $form->text('document_series', __('Document series'));
        $form->text('document_number', __('Document number'));
        $form->datetime('document_issue_date', __('Document issue date'))->default(date('Y-m-d H:i:s'));
        $form->text('document_issue_by', __('Document issue by'));
        $form->text('tachograph_brand', __('Tachograph brand'));
        $form->text('tachograph_model', __('Tachograph model'));
        $form->text('tachograph_series', __('Tachograph series'));
        $form->number('gas_equipment_number', __('Gas equipment number'));
        $form->datetime('gas_equipment_next_survey', __('Gas equipment next survey'))->default(date('Y-m-d H:i:s'));
        $form->number('gas_cylinder_serial_number', __('Gas cylinder serial number'));
        $form->datetime('gas_cylinder_date_last_survey', __('Gas cylinder date last survey'))->default(date('Y-m-d H:i:s'));
        $form->datetime('gas_cylinder_date_next_survey', __('Gas cylinder date next survey'))->default(date('Y-m-d H:i:s'));
        $form->number('gas_cylinder_year_issue', __('Gas cylinder year issue'));
        $form->text('car_type', __('Car type'));
        $form->text('status', __('Status'));
        $form->decimal('price', __('Price'));

        return $form;
    }
}
