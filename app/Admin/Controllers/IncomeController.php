<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Confirm;
use App\Admin\Widget\FilterNav\Nav;
use App\Models\Income;
use App\Models\PaymentMethod;
use App\Models\Region;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Grid\Filter;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Tab;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Route;

class IncomeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Income';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Income());

        $grid->filter(function(Filter $filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->equal('status', 'Статус')->select([
                '0' => 'Не подтвержденные',
                '1' => 'Подтвержденные'
            ]);
        });

        $grid->column('id', __('Id'));
        $grid->column('payment.user.id', 'ID агента')->link(function () {
            return route('admin.users.show', [
                'user' => $this->payment->user->id
            ], 'self');
        }, '_self');
        /*$grid->column('payment_id', __('Payment id'));
        ;
        $grid->column('comment', __('Comment'));
        */
        $grid->column('created_at', 'Дата создания')->display(function () {
            return (string) $this->created_at;
        });
        $grid->column('payment.amount', 'Сумма');
        $grid->column('date', 'Дата оплаты');
        $grid->column('paymentMethod.name', 'Способ оплаты');
        $grid->column('screen.url', 'Скрин')->display(function () {
            return 'Смотреть';
        })->modal('Скрин', function ($model) {
            return '<div style="text-align: center">
                <img src="' . $model->screen->url . '" style="max-height: 600px"></div>';
        });

        $grid->actions(function ($actions) {
            $actions->add(new Confirm());
        });

        $grid->header(function ($query) {
            $links = new Nav('status', [
                '0' => 'Не подтвержденные',
                '1' => 'Подтвержденные',
            ]);
            return $links;
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Income::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('payment_id', __('Payment id'));
        $show->field('payment_method_id', __('Payment method id'));
        $show->field('comment', __('Comment'));
        $show->field('date', __('Date'));
        $show->field('payment.amount', __('Amount'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Income());

        $form->number('payment_id', __('Payment id'))->disable();
        //$form->number('payment_method_id', __('Payment method id'));
        $form->select('payment_method_id', __('Payment method id'))->options(
            PaymentMethod::all()->pluck('name', 'id')
        );
        $form->number('payment.amount', __('Amount'));
        $form->textarea('comment', __('Comment'));
        $form->datetime('date', __('Date'))->default(date('Y-m-d H:i:s'));


        return $form;
    }
}
