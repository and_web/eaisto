<?php


namespace App\Admin\Controllers;


use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;

class AdminController extends \Encore\Admin\Controllers\AdminController
{
    public function __construct()
    {
        Admin::js('/assets/js/admin.js');
        Admin::css('/assets/css/admin.css');
    }
}
