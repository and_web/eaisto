<?php

namespace App\Admin\Controllers;

use App\Models\Price;;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Grid\Filter;
use Encore\Admin\Show;

class PriceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Price';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Price());

        $grid->filter(function(Filter $filter) {
            $filter->disableIdFilter();
        });

        //$grid->column('id', __('Id'));
        $grid->column('licenseCategory.name', __('Category'));
        $grid->column('value', __('Price'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Price::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('license_category_id', __('Category'));
        $show->field('value', __('Price'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Price());

        $form->number('license_category_id', __('Category'))->disable();
        $form->decimal('value', __('Price'));

        return $form;
    }
}
