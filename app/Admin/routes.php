<?php

use \App\Admin\Controllers\UserController;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');

    $router->resource('/users', 'UserController');
    $router->resource('orders', 'OrderController');
    $router->resource('incomes', 'IncomeController');

    $router->resource('settings/stations', 'StationController');
    $router->resource('settings/prices', 'PriceController');
    $router->resource('settings/options', 'OptionController');
    $router->resource('settings/payment-methods', 'PaymentMethodController');
});
