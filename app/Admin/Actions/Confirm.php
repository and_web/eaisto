<?php

namespace App\Admin\Actions;

use App\Models\Income;
use App\Models\Payment;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Confirm extends RowAction
{
    public $name = 'Подтвердить';

    public function handle(Model $model)
    {
        DB::transaction(function () use ($model) {
            $model->status = Income::STATUS_DONE;
            $model->save();

            $model->payment->update([
                'status' => Payment::STATUS_DONE,
            ]);
        });

        return $this->response()->success('Success message.')->refresh();
    }

}
