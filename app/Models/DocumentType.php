<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
