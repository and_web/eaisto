<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    public function licenseCategory()
    {
        return $this->belongsTo(LicenseCategory::class);
    }
}
