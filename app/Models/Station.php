<?php


namespace App\Models;


use App\Models\Traits\ValidDatesTrait;
use Database\Factories\StationFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Station extends Model
{
    use HasFactory;
    use ValidDatesTrait;

    //protected $dateFormat = 'd.m.Y';

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function diagnosticCards()
    {
        return $this->hasMany(DiagnosticCard::class);
    }

    protected static function newFactory()
    {
        return StationFactory::new();
    }
}
