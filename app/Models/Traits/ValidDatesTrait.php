<?php


namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Model;

/**
 * Trait ValidDatesTrait
 * @package App\Models\Traits
 * @mixin Model
 */
trait ValidDatesTrait
{
    public function getDates()
    {
        return array_unique(
            array_merge(parent::getDates(), [
                'valid_from',
                'valid_to',
            ])
        );
    }

    public function getValidFromAttribute()
    {
        return (new \DateTime($this->attributes['valid_from']))->format('d.m.Y');
    }

    public function getValidToAttribute()
    {
        return (new \DateTime($this->attributes['valid_to']))->format('d.m.Y');
    }
}
