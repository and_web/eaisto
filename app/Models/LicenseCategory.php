<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LicenseCategory extends Model
{
    public const CATEGORY_A = 'A';

    public const CATEGORY_B = 'B';

    public const CATEGORY_C = 'C';

    public const CATEGORY_D = 'D';

    public const CATEGORY_E = 'E';

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
