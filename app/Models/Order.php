<?php


namespace App\Models;


use Aigletter\LaravelAttachment\AttachmentTrait;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use AttachmentTrait;

    protected $casts = [
        'price' => 'float',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'document_issue_date',
        'gas_equipment_next_survey',
        'gas_cylinder_date_last_survey',
        'gas_cylinder_date_next_survey'
    ];

    protected $fillable = [
        "user_id",
        "status",
        "first_name",
        "middle_name",
        "last_name",
        "phone",
        "company",
        "vin",
        "year",
        "brand",
        "model",
        "body",
        "chassis",
        "mileage",
        "power",
        "numberplate",
        "license_category_id",
        "technical_category_id",
        "tires",
        "net_weight",
        "max_weight",
        "fuel_type_id",
        "document_type_id",
        "brake_type_id",
        "price",
        "document_series",
        "document_number",
        "document_issue_date",
        "document_issue_by",
        "tachograph_brand",
        "tachograph_model",
        "tachograph_series",
        "gas_equipment_number",
        "gas_equipment_next_survey",
        "gas_cylinder_serial_number",
        "gas_cylinder_date_last_survey",
        "gas_cylinder_date_next_survey",
        "gas_cylinder_year_issue",
        "car_type"
    ];

    /**
     * Черновик
     */
    public const STATUS_DRAFT = 'draft';

    /**
     * Ожидает оплаты
     */
    public const STATUS_PENDING = 'pending';

    /**
     * Зарегистрирован
     */
    public const STATUS_DONE = 'done';

    /**
     * Удален
     */
    public const STATUS_DELETED = 'deleted';

    public const CAR_TYPE_REGULAR = 'regular';

    public const CAR_TYPE_PASSENGER = 'passenger';

    public const CAR_TYPE_TRUCK = 'truck';

    public const CAR_TYPE_DANGEROUS ='dangerous';

    public const CAR_TYPE_TRAINING ='training';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fuelType()
    {
        return $this->belongsTo(FuelType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brakeType()
    {
        return $this->belongsTo(BrakeType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function licenseCategory()
    {
        return $this->belongsTo(LicenseCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function technicalCategory()
    {
        return $this->belongsTo(TechnicalCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function documentType()
    {
        return $this->belongsTo(DocumentType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function diagnosticCard()
    {
        return $this->hasOne(DiagnosticCard::class);
    }

    /**
     * @return string|null
     */
    public function getStatusNameAttribute()
    {
        return __($this->status);
    }

    /*public function getPdfAttribute()
    {
        return $this->diagnosticCard()->first();
    }*/

    public function outcome()
    {
        return $this->hasOne(Outcome::class);
    }
}
