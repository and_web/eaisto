<?php


namespace App\Models;

use Aigletter\LaravelAttachment\AttachmentTrait;
use Aigletter\LaravelAttachment\Models\Attachment;
use App\Models\Traits\ValidDatesTrait;
use Illuminate\Database\Eloquent\Model;

class DiagnosticCard extends Model
{
    use ValidDatesTrait;
    use AttachmentTrait;

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function station()
    {
        return $this->belongsTo(Station::class);
    }

    public function pdf()
    {
        return $this->morphOne(Attachment::class, 'attachmentable');
    }
}
