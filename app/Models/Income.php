<?php


namespace App\Models;


use Aigletter\LaravelAttachment\AttachmentTrait;
use Aigletter\LaravelAttachment\Models\Attachment;
use App\Models\Traits\RelatedListTrait;
use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    use AttachmentTrait;

    public const STATUS_PENDING = 0;

    public const STATUS_DONE = 1;

    protected $fillable = [
        'user_id',
        'payment_id',
        'payment_method_id',
        'comment',
        'date'
    ];

    //protected $dateFormat = 'd.m.Y H:i:s';

    public function getCreatedAtColumn()
    {
       return parent::getCreatedAtColumn();
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function screen()
    {
        return $this->morphOne(Attachment::class, 'attachmentable');
    }
}
