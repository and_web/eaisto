<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Outcome extends Model
{
    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
