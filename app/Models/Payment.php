<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public const TYPE_INCOME = 'income';

    public const TYPE_OUTCOME = 'outcome';

    public const STATUS_PENDING = 0;

    public const STATUS_DONE = 1;

    protected $fillable = [
        'user_id',
        'type',
        'amount',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * @todo
     */
    public function income()
    {
        return $this->hasOne(Income::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * @todo
     */
    public function outcome()
    {
        return $this->hasOne(Outcome::class);
    }
}
