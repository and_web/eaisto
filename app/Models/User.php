<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    public const STATUS_REGISTERED = 1;
    public const STATUS_VERIFIED = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'first_name',
        'last_name',
        'middle_name',
        'region_id',
        'city',
        'need',
        'insurance_company',
        'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function getStatuses()
    {
        return [
            'registered' => self::STATUS_REGISTERED,
            'verified' => self::STATUS_VERIFIED,
        ];
    }

    public function changeStatus(int $status)
    {
        if (!in_array($status, $this->getStatuses())) {
            throw new \Exception('Unknown status');
        }

        $this->status = $status;
        $this->save();
    }

    public function getStatus()
    {
        $statuses = $this->getStatuses();
        foreach ($statuses as $key => $value) {
            if ($this->status === $value) {
                return $key;
            }
        }

        return null;
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class)->where('status', 1);
    }

    public function getBalanceAttribute()
    {
        return (float) $this->payments()->sum('amount');
    }
}
