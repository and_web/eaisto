<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    public function paymentPaymentMethod()
    {
        return $this->hasMany(Income::class);
    }
}
