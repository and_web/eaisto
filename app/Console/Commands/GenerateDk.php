<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Services\Registrator\DkGenerator;
use Illuminate\Console\Command;

class GenerateDk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:dk {order?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate PDF';

    protected $generator;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DkGenerator $generator)
    {
        parent::__construct();

        $this->generator = $generator;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('order') ?: 29;
        $order = Order::query()->where('id', $id)->first();
        $this->generator->generate($order);

        return 0;
    }
}
