<?php


namespace App\View\Components;


use Illuminate\View\Component;

class Step extends Component
{
    protected $step;

    protected $svg;

    protected $text;

    protected $active;

    public function __construct($step, $text, $svg, $active = false)
    {
        $this->text = $text;
        $this->svg = $svg;
        $this->icon = $svg;
        $this->active = $active;
    }

    public function render()
    {
        $svg = file_get_contents(public_path($this->svg));
        return view('components.step', [
            'step' => $this->step,
            'svg' => $svg,
            'text' => $this->text,
            'class' => $this->active ? 'active' : ''
        ]);
    }
}
