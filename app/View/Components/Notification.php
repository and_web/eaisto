<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Notification extends Component
{
    public const TYPE_SUCCESS = 'success';

    public const TYPE_ERROR = 'error';

    protected $type;

    protected $message;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($message, $type = self::TYPE_SUCCESS)
    {
        if (is_array($message)) {
            $type = $message['type'] ?? $type;
            $message = $message['message'];
        }

        $this->message = $message;
        $this->type = $type;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.notification', [
            'type' => $this->type,
            'message' => $this->message
        ]);
    }
}
