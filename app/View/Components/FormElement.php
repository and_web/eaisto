<?php


namespace App\View\Components;


use Illuminate\View\Component;

class FormElement extends Component
{

    public function render()
    {
        return view('components.form-element');
    }
}
