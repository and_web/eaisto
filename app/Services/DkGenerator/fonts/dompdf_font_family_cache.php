<?php

$t = '';
return [
    'times new roman' => [
        'normal' => $fontDir . '/TimesNewRomanCyr.ttf',
        'bold' => $fontDir . '/TimesNewRomanCyrBold.ttf'
    ],
];
