<?php


namespace App\Services\Registrator;


use Aigletter\LaravelAttachment\LaravelAttachment;
use App\Models\DiagnosticCard;
use App\Models\LicenseCategory;
use App\Models\Order;
use App\Models\Station;
use App\Models\User;
use Barryvdh\DomPDF\PDF;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;

class DkGenerator
{
    protected $template = '{operator}{expert}{year}{number}';

    protected $generator;

    protected $attachment;

    public function __construct(PDF $generator, LaravelAttachment $attachment)
    {
        $this->generator = $generator;
        $this->attachment = $attachment;
    }

    public function generate($order)
    {
        $user = User::query()->where('id', $order->user_id)->first();
        $station = $this->getRandomRegionStation($user->region_id) ?? $this->getRandomStation();
        $startDate = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
        $endDate = $this->calculateEndDate($order, $startDate);
        $number = $this->generateCardNumber($station, $startDate);

        $view = view('generator.template', [
            'number' => $number,
            'startDate' => $startDate->format('d.m.Y'),
            'endDate' => $endDate->format('d.m.Y'),
            'station' => $station,
            'order' => $order,
        ]);
        $html = $view->toHtml();
        $this->generator->loadHTML($html);
        $filename = '/tmp/' . $number . '.pdf';
        $this->generator->save($filename);
        $file = new File($filename);

        DB::beginTransaction();

        try {
            $card = new DiagnosticCard();
            $card->valid_from = $startDate;
            $card->valid_to = $endDate;
            $card->number = $number;
            $card->order_id = $order->id;
            $station->diagnosticCards()->save($card);

            $this->attachment->attach($file, $card, 'diagnostic_card');
            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        return true;
    }

    protected function calculateEndDate($order, ?\DateTimeInterface $startDate = null)
    {
        //$startDate = $startDate ?? $this->getCurrentDate();\
        $startDate = $startDate ?? $this->getCurrentDate();
        $years = $startDate->format('Y') - $order->year;

        if ($this->isFirstCategory($order)) {
            $term = $this->termForFirstCategory($years);
        } elseif ($this->isSecondCategory($order)) {
            $term = $this->termForSecondCategory($years);
        } elseif ($this->isThirdCategory($order)) {
            $term = $this->termForThirdCategory($years);
        } else {
            throw new \Exception('Can not define term for current data');
        }

        $endDate = clone $startDate;
        $endDate->modify('+' . $term . ' years');

        return $endDate;
    }

    protected function isFirstCategory($order)
    {
        // Легковые
        if ($order->car_type === Order::CAR_TYPE_REGULAR && $order->licenseCategory->name === LicenseCategory::CATEGORY_B) {
            return true;
        }

        // Грузовые меньше 3500
        if ($order->car_type === Order::CAR_TYPE_TRUCK && $order->max_weight < 3500) {
            return true;
        }

        // Мототехника
        if ($order->licenseCategory->name === LicenseCategory::CATEGORY_A) {
            return true;
        }

        return false;
    }

    protected function isSecondCategory($order)
    {
        // Такси, автобусы
        if ($order->car_type === Order::CAR_TYPE_PASSENGER) {
            return true;
        }

        // Грузовики более 3500
        if ($order->car_type === Order::CAR_TYPE_TRUCK && $order->max_weight > 3500) {
            return true;
        }

        // Учебные
        if ($order->car_type === Order::CAR_TYPE_TRAINING) {
            return true;
        }

        return false;
    }

    protected function isThirdCategory($order)
    {
        return $order->car_type === Order::CAR_TYPE_DANGEROUS;
    }


    protected function termForFirstCategory($years)
    {
        // TODO в законе не надо
        if ($years < 4) {
            return 3;
        }

        if ($years >= 4 && $years <= 10) {
            return 2;
        }

        return 1;
    }

    protected function termForSecondCategory($years)
    {
        if ($years <= 5) {
            return 1;
        }

        return 0.5;
    }

    protected function termForThirdCategory($years)
    {
        return 0.5;
    }

    /*protected function calculateEndDate($order, ?\DateTimeInterface $startDate = null)
    {
        $startDate = $startDate ?? $this->getCurrentDate();

        if ($order->car_type === Order::CAR_TYPE_PASSENGER) {
            $years = $this->calculateForPassenger($order->year, $startDate);
        } elseif ($order->licenseCategory->name === LicenseCategory::CATEGORY_C && $order->max_weight > 3500) {
            $years = $this->calculateForTruck($order->year, $startDate);
        } else {
            $years = $this->calculateForRegular($order->year, $startDate);
        }

        $endDate = clone $startDate;
        $endDate->modify('+' . $years . ' years');

        return $endDate;
    }

    protected function calculateForPassenger($year, ?\DateTimeInterface $startDate = null)
    {
        $startDate = $startDate ?? $this->getCurrentDate();
        $years = $startDate->format('Y') - $year;

        // На 6 месяцев для такси и автобусов более 5 лет
        if ($years >= 5) {
            return 0.5;
        }

        // а также 1 год для такси и автобусов до 5 лет выпуска
        return 1;
    }

    protected function calculateForTruck($year, ?\DateTimeInterface $startDate = null)
    {
        // больше 10 - 1 год, С,
        return 1;
    }

    protected function calculateForRegular($year, ?\DateTimeInterface $startDate = null)
    {
        $startDate = $startDate ?? $this->getCurrentDate();
        $years = $startDate->format('Y') - $year;

        // до 3 - 3 года
        if ($years < 3) {
            return 3;
        }

        // от 3 до 10 - 2 года А, В, Е а также категория С с грузоподъемностью до 3.5 тонн
        if ($years >= 3 && $years < 10) {
            return 2;
        }

        // больше 10 - 1 год, С,
        return 1;
    }*/

    protected function getCurrentDate()
    {
        return new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
    }

    protected function generateCardNumber(Station $station, ?\DateTimeInterface $startDate = null)
    {
        // RSA NUMBER
        $rsaId = str_pad($station->rsa_id, 5, '0', STR_PAD_LEFT);

        // EXPERT NUMBER
        $expertId = str_pad($station->expert_id, 3, '0', STR_PAD_LEFT);

        // YEAR
        $startDate = $startDate ??  new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
        $year = $startDate->format('y');

        // SERIAL NUMBER
        $lastNumber = $this->getLastNumber($station);
        if ($lastNumber) {
            $lastSerialNumber = substr($lastNumber, -5);
        } else {
            $lastSerialNumber = $station->start_id;
        }
        $serialNumber = $lastSerialNumber + 1;
        $serialNumber = str_pad($serialNumber, 5, '0', STR_PAD_LEFT);

        $number = $rsaId . $expertId . $year . $serialNumber;

        return $number;
    }

    /**
     * @param $regionId
     * @return Station
     */
    protected function getRandomRegionStation($regionId)
    {
        $stations = Station::query()->where('region_id', $regionId)->get();
        return $stations->count() ? $stations->random() : null;
    }

    protected function getRandomStation()
    {
        $stations = Station::query()->get();
        return $stations->random();
    }

    protected function getLastNumber(Station $station)
    {
        $lastCard = $this->getLastDiagnosticCard($station);

        return $lastCard->number ?? null;
    }

    protected function getLastDiagnosticCard(Station $station)
    {
        return $station->diagnosticCards()->orderBy('created_at', 'desc')->first();
    }
}
