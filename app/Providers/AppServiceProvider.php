<?php

namespace App\Providers;

use Aigletter\LaravelAttachment\LaravelAttachment;
use App\Services\Registrator\DkGenerator;
use Database\Doctrine\Enum;
use Database\Doctrine\TinyInteger;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DkGenerator::class, function(Application $app){
            $generator = $app->make('dompdf.wrapper');
            $attachment = $app->get(LaravelAttachment::class);
            return new DkGenerator($generator, $attachment);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::registerCustomDoctrineType(TinyInteger::class, TinyInteger::NAME, 'TINYINT');
        Schema::registerCustomDoctrineType(Enum::class, Enum::NAME, 'ENUM');
    }
}
