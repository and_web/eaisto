<?php


namespace App\Http\Controllers;


use App\Http\Requests\UpdateUserRequest;
use App\Models\Region;
use App\Models\User;

class UserController
{
    public function show()
    {
        return view('user-show', [
            'user' => auth()->user(),
        ]);
    }

    public function update($id, UpdateUserRequest $request)
    {
        $data = $request->validated();
        $user = User::query()->where('id', $id)->first();
        $user->fill($data);
        $user->save();

        return redirect(route('user.show', ['id' => $id]))
            ->with('notification', 'Ваши изменения успешно сохранены');
    }

    public function edit()
    {
        $regions = Region::all();
        return view('user-edit', [
            'user' => auth()->user(),
            'regions' => $regions,
        ]);
    }
}
