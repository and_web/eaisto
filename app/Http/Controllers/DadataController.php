<?php


namespace App\Http\Controllers;


use App\Models\Region;
use Illuminate\Http\Request;

class DadataController
{
    public function find(Request $request)
    {
        $query = $request->input('query');
        $region = $request->input('region');

        $token = "810ac2249319d6edb7d6e6e4632b2c80a2b8fac3";
        $dadata = new \Dadata\DadataClient($token, null);
        $params = [
            "from_bound" =>["value" => "city"],
            "to_bound" => ["value" => "city"],
        ];
        if ($region) {
            $region = Region::query()->where('id', $region)->first();
            $params['locations'] = [
                [
                    'region' => trim(
                        preg_replace('/(республика|край)/iu', '', $region->name)
                    )
                ]
            ];
        }
        $responses = $dadata->suggest("address", $query, 20, $params);

        $results = [];
        foreach ($responses as $response) {
            if ($response['data']['fias_level']  == 4) {
                $results[] = [
                    'id' => $response['value'],
                    'text' => $response['value'],
                ];
            }
        }
        return $results;


    }
}
