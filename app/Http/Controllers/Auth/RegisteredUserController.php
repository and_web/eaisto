<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\StoreUserRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create()
    {
        $regions = Models\Region::all();
        return view('auth.register', [
            'regions' => $regions,
        ]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StoreUserRequest $request)
    {
        $data = array_merge($request->validated(), [
            'password' => Hash::make($request->password),
            'status' => User::STATUS_REGISTERED,
        ]);

        $user = User::create($data);

        event(new Registered($user));

        Auth::login($user);

        return redirect(route('home'))->with(
            'notification',
            'Спасибо за вашу регистрацию. Пожалуйста, подтвердите ваш емейл.
                    Если письмо не пришло в течение 10 минут, то проверьте папку Спам.'
        );
    }
}
