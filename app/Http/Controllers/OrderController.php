<?php

namespace App\Http\Controllers;

use Aigletter\LaravelAttachment\LaravelAttachment;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\BrakeType;
use App\Models\DocumentType;
use App\Models\FuelType;
use App\Models\LicenseCategory;
use App\Models\Order;
use App\Models\Outcome;
use App\Models\Payment;
use App\Models\OrderPayment;
use App\Models\Price;
use App\Models\TechnicalCategory;
use App\Models\User;
use App\Services\Registrator\DkGenerator;
use App\View\Components\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::query()->with('diagnosticCard.pdf')
            ->where('user_id', auth()->id())->paginate(100);
        return view('order-list', [
            'orders' => $orders,
            'user' => auth()->user(),
        ]);
    }

    public function create()
    {
        $licenceCategories = LicenseCategory::query()->get();
        $technicalCategories = TechnicalCategory::query()->get();
        $fuelTypes = FuelType::query()->get();
        $brakeTypes = BrakeType::query()->get();
        $documentTypes = DocumentType::query()->get();
        return view('order-create', [
            'licenceCategories' => $licenceCategories,
            'technicalCategories' => $technicalCategories,
            'fuelTypes' => $fuelTypes,
            'brakeTypes' => $brakeTypes,
            'documentTypes' => $documentTypes,
        ]);
    }

    public function edit($id)
    {
        $order = Order::query()->where('id', $id)->first();

        if ($order->status != Order::STATUS_PENDING && $order->status !== Order::STATUS_DRAFT) {
            return redirect(route('order.index'))->with('notification', [
                'type' => 'error',
                'message' => 'Данный заказ нельзя редактировать'
            ]);
        }

        $licenceCategories = LicenseCategory::query()->get();
        $technicalCategories = TechnicalCategory::query()->get();
        $fuelTypes = FuelType::query()->get();
        $brakeTypes = BrakeType::query()->get();
        $documentTypes = DocumentType::query()->get();
        return view('order-create', [
            'licenceCategories' => $licenceCategories,
            'technicalCategories' => $technicalCategories,
            'fuelTypes' => $fuelTypes,
            'brakeTypes' => $brakeTypes,
            'documentTypes' => $documentTypes,
            'order' => $order,
        ]);
    }

    public function store(StoreOrderRequest $request, LaravelAttachment $attachment)
    {
        $price = Price::query()->where('license_category_id', $request->license_category_id)->first();
        $data = array_merge($request->validated(), [
            'user_id' => auth()->id(),
            'status' => $request->submit === 'save' ? Order::STATUS_PENDING : Order::STATUS_DRAFT,
            'price' => $price->value,
        ]);

        DB::beginTransaction();
        try {
            $order = Order::create($data);

            $attachment->attach($request->file('front_car_photo'), $order, 'front_car_photo');
            $attachment->attach($request->file('back_car_photo'), $order, 'back_car_photo');

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        if ($order->status === Order::STATUS_DRAFT) {
            return redirect(route('order.index'))->with('notification', 'Черновик сохранен');
        }

        return redirect(route('order.pay', [
            'id' => $order->id
        ]));
    }

    public function update($id, UpdateOrderRequest $request, LaravelAttachment $attachment)
    {
        $price = Price::query()->where('license_category_id', $request->license_category_id)->first();
        $data = array_merge($request->validated(), [
            'user_id' => auth()->id(),
            'status' => $request->submit === 'save' ? Order::STATUS_PENDING : Order::STATUS_DRAFT,
            'price' => $price->value,
        ]);

        $order = Order::query()->where('id', $id)->first();
        $order->fill($data);

        if ($file = $request->file('front_car_photo')) {
            $attachment->attach($file, $order, 'front_car_photo');
        }
        if ($file = $request->file('back_car_photo')) {
            $attachment->attach($file, $order, 'back_car_photo');
        }

        $order->save();

        if ($order->status === Order::STATUS_DRAFT) {
            return redirect(route('order.index'))->with('notification', 'Черновик сохранен');
        }

        return redirect(route('order.pay', [
            'id' => $order->id
        ]));
    }

    public function pay($id)
    {
        $order = Order::query()->where('id', $id)->first();

        if (!$order) {
            return redirect(route('order.index'));
        }

        if (! Gate::allows('order-owner', $order)) {
            abort(403);
        }

        $user = auth()->user();
        return view('order-pay', [
            'order' => $order,
            'user' => $user
        ]);
    }

    public function register(Request $request, DkGenerator $dkGenerator)
    {
        $request->validate([
            'order_id' => 'bail|required|exists:orders,id',
        ]);

        /** @var Order $order */
        $order = Order::query()->where('id', $request->order_id)->first();

        if (! Gate::allows('order-owner', $order)) {
            abort(403);
        }

        // TODO Расскоментироват после тестирования
        if ($order->status === Order::STATUS_DONE) {
            return redirect(route('order.receipt', [
                'id' => $order->id,
            ]))->with('notification', 'Этот заказ уже был оплачен ранее');
        }

        /** @var User $user */
        $user = auth()->user();
        if ($user->balance < $order->price) {
            return redirect()->back()->with('notification', [
                'type' => Notification::TYPE_ERROR,
                'message' => 'На вашем счете недостаточно средств для оплаты данного заказа',
            ]);
        }

        DB::beginTransaction();
        try {
            $payment = new Payment();
            $payment->type = Payment::TYPE_OUTCOME;
            $payment->amount = -$order->price;
            $payment->status = Payment::STATUS_DONE;
            $user->payments()->save($payment);

            $outcome = new Outcome();
            $outcome->payment_id = $payment->id;
            $outcome->order_id = $order->id;
            $outcome->save();

            $order->status = Order::STATUS_DONE;
            $order->save();

            $dkGenerator->generate($order);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        return redirect(route('order.receipt', [
            'id' => $request->order_id,
        ]));
    }

    public function receipt($id)
    {
        $order = Order::query()->where('id', $id)->first();

        if (!$order) {
            return redirect(route('order.index'));
        }

        if (! Gate::allows('order-owner', $order)) {
            abort(403);
        }

        return view('order-receipt', [
            'order' => $order,
        ]);
    }
}
