<?php


namespace App\Http\Controllers;

use App\Models;


class HomeController extends Controller
{
    public function index()
    {
        $regions = Models\Region::all();
        $stations = Models\Station::all();
        return view('index', [
            'regions' => $regions,
            'stations' => $stations,
        ]);
    }

}
