<?php


namespace App\Http\Controllers;


use Aigletter\LaravelAttachment\LaravelAttachment;
use App\Models\Income;
use App\Models\Payment;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = Payment::query()->with(['income', 'outcome'])->where([
            'user_id' => auth()->id(),
            'status' => Payment::STATUS_DONE,
        ])->orderBy('created_at', 'desc')->get();
        return view('payment-index', [
            'payments' => $payments,
        ]);
    }

    public function create()
    {
        $paymentMethods = PaymentMethod::query()->get();
        return view('payment-create', [
            'paymentMethods' => $paymentMethods,
        ]);
    }

    public function store(Request $request, LaravelAttachment $uploader)
    {
        $request->validate([
            'amount' => 'required|int|max:1000000',
            'date' => 'required|date',
            'payment_method_id' => 'required|int|exists:payment_methods,id',
            'payment_receipt' => 'required|image',
            'comment' => 'string|max:500'
        ]);

        DB::beginTransaction();
        try {
            $payment = Payment::create([
                'user_id' => auth()->id(),
                'type' => Payment::TYPE_INCOME,
                'amount' => $request->amount,
                'status' => Payment::STATUS_PENDING,
            ]);

            $income = Income::create([
                'user_id' => auth()->id(),
                'payment_id' => $payment->id,
                'payment_method_id' => $request->payment_method_id,
                'comment' => $request->comment,
                'date' => $request->date
            ]);

            $uploader->attach($request->file('payment_receipt'), $income, 'payment_receipt');

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        return redirect()->back()->with(
            'notification',
            'Спасибо за вашу оплату. Ваш баланс будет пополнен в рабочие часы по Мск. в течение 1 часа'
        );
    }
}
