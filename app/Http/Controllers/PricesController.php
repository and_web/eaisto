<?php


namespace App\Http\Controllers;


use App\Models\Price;

class PricesController extends Controller
{
    public function index()
    {
        $prices = Price::query()->with('licenseCategory')->get();
        return view('prices', [
            'prices' => $prices,
        ]);
    }
}
