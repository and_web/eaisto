<?php


namespace App\Http\Controllers;


use App\Models\DiagnosticCard;
use App\Models\Order;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class CheckController extends Controller
{
    public function index(Session $session)
    {
        $order = $session->get('order');
        return view('check', [
            'order' => $order,
        ]);
    }

    public function search(Request $request)
    {
        $rules = [
            'numberplate' => 'required_without_all:vin,body,chassis,number',
            'vin' => 'required_without_all:numberplate,body,chassis,number',
            'body' => 'required_without_all:numberplate,vin,chassis,number',
            'chassis' => 'required_without_all:numberplate,vin,body,number',
            'number' => 'required_without_all:numberplate,vin,body,chassis',
        ];

        $request->validate($rules);

        foreach (array_keys($rules) as $field) {
            if ($request->{$field}) {
                break;
            }
        }

        $order = null;
        if ($field !== 'number') {
            $order = Order::query()->with('diagnosticCard')->where($field, $request->{$field})->first();
        } else {
            $card = DiagnosticCard::query()->where('number', $request->{$field})->first();
            if ($card) {
                $order = $card->order()->with('diagnosticCard')->first();
            }
        }

        return redirect()->route('check.index')->with('order', $order);
    }
}
