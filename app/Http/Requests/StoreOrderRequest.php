<?php

namespace App\Http\Requests;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'bail|required|string|min:2|max:50',
            'middle_name' => 'bail|required|string|min:2|max:50',
            'last_name' => 'bail|required|string|min:2|max:50',
            'phone' => 'bail|nullable|string|min:10|max:20',
            'company' => 'bail|nullable|string|min:2|max:25',
            'vin' => 'bail|nullable|string|min:17|max:17',
            'year' => 'bail|required|string|min:4|max:4',
            'brand' => 'bail|required|string|min:2|max:25',
            'model' => 'bail|required|string|min:2|max:20',
            'body' => 'bail|string|min:2|max:20',
            'chassis' => 'bail|string|min:2|max:20',
            'mileage' => 'bail|required|numeric',
            'power' => 'bail|required|numeric|min:1',
            'numberplate' => 'bail|nullable|string|min:4|max:20',
            'license_category_id' => 'bail|required|exists:license_categories,id',
            'technical_category_id' => 'bail|required|exists:technical_categories,id',
            'tires' => 'bail|required|string|min:2|max:25',
            'net_weight' => 'bail|required|numeric|min:500',
            'max_weight' => 'bail|required|numeric|min:500',
            'brake_type_id' => 'bail|required|numeric|min:1|max:10',
            'fuel_type_id' => 'bail|required|numeric|min:1|max:10',
            'document_type_id' => 'bail|required|numeric|min:1|max:10',

            'document_series' => 'exclude_if:,3|bail|nullable|string|min:2|max:50',

            'document_number' => 'bail|required|string|min:6|max:16',
            'document_issue_date' => 'bail|required|date',
            'document_issue_by' => 'bail|required|string|min:2|max:50',
            'tachograph_brand' => 'bail|nullable|string|min:2|max:50',
            'tachograph_model' => 'bail|nullable|string|min:2|max:50',
            'tachograph_series' => 'bail|nullable|string|min:2|max:50',
            'gas_equipment_number' => 'bail|nullable|numeric|min:2|max:50',
            'gas_equipment_next_survey' => 'bail|nullable|date_format:d-m-Y',
            'gas_cylinder_serial_number' => 'bail|nullable|numeric|min:2|max:50',
            'gas_cylinder_date_last_survey' => 'bail|nullable|date_format:d-m-Y',
            'gas_cylinder_date_next_survey' => 'bail|nullable|date_format:d-m-Y',
            'gas_cylinder_year_issue' => 'bail|nullable|date_format:Y',
            'car_type' => [
                'required',
                Rule::in([
                    Order::CAR_TYPE_DANGEROUS,
                    Order::CAR_TYPE_PASSENGER,
                    Order::CAR_TYPE_REGULAR,
                    Order::CAR_TYPE_TRUCK,
                    Order::CAR_TYPE_TRAINING,
                ]),
            ],
            'front_car_photo' => 'required|image',
            'back_car_photo' => 'required|image'
        ];
    }
}
