<?php


namespace App\Http\Requests;


use App\Http\Requests\Auth\StoreUserRequest;

class UpdateUserRequest extends StoreUserRequest
{
    public function authorize()
    {
        return $this->route('id') == auth()->id();
    }

    public function rules()
    {
        $rules = array_filter(parent::rules(), function ($key) {
            return !in_array($key, [
                'email',
                'password',
                'insurance_company',
                'need',
                'phone',
            ]);
        }, ARRAY_FILTER_USE_KEY);

        return $rules;
    }


}
