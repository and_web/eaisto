<?php


namespace App\Http\Requests\Auth;


use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'bail|required|string|email|max:255|unique:users',
            'password' => 'bail|required|string|confirmed|min:6',
            'first_name' => 'bail|required|string|min:2|max:255',
            'last_name' => 'bail|required|string|min:2|max:255',
            'middle_name' => 'bail|required|string|min:2|max:255',
            'region_id' => 'bail|required|exists:regions,id',
            'city' => 'bail|required|string|min:2|max:255',
            'need' => 'bail|required|integer|min:1|max:100000',
            'insurance_company' => 'bail|required|string|min:2|max:255',
            'phone' => 'bail|required|string|min:10|max:20',
        ];
    }
}
