jQuery(document).ready(function($){
    (function(){
        var defaultParams = {
            theme: "bootstrap4",
            minimumInputLength: 2,
            disabled: false,
            ajax: {
                url: "/api/dadata/find",
                type: 'post',
                dataType: 'json',
                data: function (params) {
                    return  {
                        query: params.term,
                        region: $('#region_id').val(),
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
            }
        };
        var select =  $('.dadata').select2(defaultParams);

        $('#region_id').on('change', function () {
            if (['77','78','85'].includes($(this).val())) {
                var text = $(this).find('option:selected').text();
                var newOption = new Option(text, text, true, true);
                select.append(newOption).trigger('change');
                select.attr('disabled', 'disabled');
                //select.attr('readonly', 'readonly');
            } else {
                //select.removeAttr('readonly');
                select.removeAttr('disabled');
                select.val(null).trigger('change');
            }
        });

        $('#region_id').parents('form').on('submit', function(){
            select.removeAttr('disabled');
        });

        $('.dropdown-select2').select2();

        $('[data-payment-method-id]').on('click', function () {
            var method = $(this).attr('data-payment-method-id');
            $('[name=payment_method_id]').val(method);
        });
    })()

    $('#phone').mask('+7(000)000-00-00');


})

//show and hide password
$('body').on('click', '.password-control, .password-confirm-control', function(){
    var passwordElement = $(this).parent().find('input');
    if (passwordElement.attr('type') === 'password'){
        $(this).addClass('view');
        passwordElement.attr('type', 'text');
    } else {
        $(this).removeClass('view');
        passwordElement.attr('type', 'password');
    }
    return false;
});

//bootstrap validation
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

//add dadata service
/*var url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
var token = "810ac2249319d6edb7d6e6e4632b2c80a2b8fac3";
var query = document.getElementById('city').value;

var options = {
    method: "POST",
    mode: "cors",
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Token " + token
    },
    body: JSON.stringify({query: query})
}

fetch(url, options)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log("error", error));*/

//to UPPER
function changeToUpperCase(el)
{
    el.value =el.value.trim().toUpperCase();
}

//download file button
$(document).ready( function() {
    $("#fl_inp").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $("#fl_nm").html(filename);
    });
});
$(document).ready( function() {
    $("#fl_inp2").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $("#fl_nm2").html(filename);
    });
});

$('#modal').modal();

/*$(document).ready(function() {
    $(".question").on('click', function () {
        $(this).siblings(".answer").toggleClass("hide");
        $(this).toggleClass("yellow");

    });
});*/

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
$('.form-group').tooltip({ boundary: 'window' });

$(document).ready(function() {
    $("#legal_entity").on('click', function () {
        $("#block-company").toggleClass("hide");
    });
    $("#gas_equipment").on('click', function () {
        $(".gas_equipment").toggleClass("hide");
    });
});
$(document).ready(function() {
    $(".show_region_list").on('click', function () {
        $(".table-order-list").toggleClass("hide");
    });
    if (document.querySelector('.show_region_list')) {
        const btn = document.querySelector('.show_region_list');
        btn.addEventListener('click', function() {
            btn.innerHTML =
                (btn.innerHTML === 'Посмотреть весь список') ? btn.innerHTML = 'Свернуть список' : btn.innerHTML = 'Посмотреть весь список';
        });
    }
});




////////////////////////////////////////////////////////////////////
/*$(document).ready.getElementsById('email').onkeyup = function() {
    var reg = /[а-яА-ЯёЁ]/g;
    if (this.value.search(reg) !=  -1) {
        this.value  =  this.value.replace(reg, '');
    }
}

el.addEventListener('input', function() {
    this.value = this.value.replace(/[^а-яё\s]/ig, '');
});

function nooo(){
    var reg=/[a-zA-Z]/;
    if(reg.test(this.value)){
        this.value=(this.value.replace(reg, ''));
    } else {
        this.value=changeToUpperCase(this.value)
    }
}*/




