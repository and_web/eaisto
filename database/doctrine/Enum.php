<?php


namespace Database\Doctrine;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class Enum extends Type
{
    /**
     * The name of the custom type.
     *
     * @var string
     */
    public const NAME = 'enum';

    public function getSQLDeclaration(array $column, AbstractPlatform $platform)
    {
        return "ENUM ('" . implode("','", $column['allowed']) . "')";
    }

    public function getName()
    {
        return self::NAME;
    }
}
