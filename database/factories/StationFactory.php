<?php

namespace Database\Factories;

use App\Models\Station;
use Illuminate\Database\Eloquent\Factories\Factory;

class StationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Station::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'rsa_id' => rand(10000, 30000),
            'name' => $this->faker->company,
            'oto' => $this->faker->address,
            'pto' => $this->faker->address,
            'expert_id' => rand(1, 999),
            'expert_name' => $this->faker->name(),
            'start_id' => rand(1000, 9999),
            'certificate' => $this->faker->md5,
            'owner' => $this->faker->name,
            'valid_from' => $this->faker->dateTimeBetween('-5 years'),
            'valid_to' => $this->faker->dateTimeBetween('now', '+2 years'),
            'region_id' => rand(1, 86),
            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),
        ];
    }
}
