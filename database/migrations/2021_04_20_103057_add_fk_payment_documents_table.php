<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkPaymentDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_documents', function (Blueprint $table){
            $table->foreign('payment_id', 'fk_payment_file_payments')->references('id')->on('payments');
            $table->foreign('file_id', 'fk_payment_file_documents')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_documents', function (Blueprint $table){
            $table->dropForeign('fk_payment_file_payments');
            $table->dropForeign('fk_payment_file_documents');
        });
    }
}

