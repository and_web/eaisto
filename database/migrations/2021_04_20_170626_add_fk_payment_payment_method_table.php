<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkPaymentPaymentMethodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_payment_method', function (Blueprint $table){
            $table->foreign('payment_id', 'fk_payment_payment_method_payments')->references('id')->on('payments');
            $table->foreign('payment_method_id', 'fk_payment_payment_method_payment_methods')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_payment_method', function (Blueprint $table){
            $table->dropForeign('fk_payment_payment_method_payments');
            $table->dropForeign('fk_payment_payment_method_payment_methods');
        });
    }
}
