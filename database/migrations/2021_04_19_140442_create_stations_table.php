<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stations', function (Blueprint $table) {
            $table->id();
            $table->text('oto');
            $table->text('pto');
            $table->integer('expert_id');
            $table->string('expert_name', 255);
            $table->integer('start_id');
            $table->string('certificate', 255);
            $table->string('owner', 255);
            $table->timestamp('valid_from')->useCurrent();
            $table->timestamp('valid_to')->useCurrent();
            $table->bigInteger('region_id', false, true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations');
    }
}
