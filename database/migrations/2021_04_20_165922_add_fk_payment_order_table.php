<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkPaymentOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_order', function (Blueprint $table){
            $table->foreign('payment_id', 'fk_payment_order_payments')->references('id')->on('payments');
            $table->foreign('order_id', 'fk_payment_order_orders')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_order', function (Blueprint $table){
            $table->dropForeign('fk_payment_order_payments');
            $table->dropForeign('fk_payment_order_orders');
        });
    }
}
