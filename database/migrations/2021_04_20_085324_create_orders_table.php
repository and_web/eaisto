<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id', false, true);
            $table->string('first_name', 255);
            $table->string('middle_name', 255);
            $table->string('last_name', 255);
            $table->string('phone', 255)->nullable();
            $table->string('company', 255)->nullable();
            $table->string('vin', 255)->nullable();
            $table->string('year', 255);
            $table->string('brand', 255);
            $table->string('model', 255);
            $table->string('body', 255)->nullable();
            $table->string('chassis', 255)->nullable();
            $table->integer('mileage');
            $table->integer('power');
            $table->string('numberplate', 255)->nullable();
            $table->bigInteger('license_category_id', false, true);
            $table->bigInteger('technical_category_id', false, true);
            $table->string('tires', 255);
            $table->integer('net_weight');
            $table->integer('max_weight');
            $table->bigInteger('fuel_type_id', false, true);
            $table->bigInteger('brake_type_id', false, true);
            $table->bigInteger('document_type_id', false, true);
            $table->string('document_series', 255)->nullable();
            $table->string('document_number', 255);
            $table->timestamp('document_issue_date');
            $table->string('document_issue_by', 255);
            $table->string('tachograph_brand', 255)->nullable();
            $table->string('tachograph_model', 255)->nullable();
            $table->string('tachograph_series', 255)->nullable();
            $table->integer('gas_equipment_number')->nullable();
            $table->timestamp('gas_equipment_next_survey')->nullable();
            $table->integer('gas_cylinder_serial_number')->nullable();
            $table->timestamp('gas_cylinder_date_last_survey')->nullable();
            $table->timestamp('gas_cylinder_date_next_survey')->nullable();
            $table->integer('gas_cylinder_year_issue')->nullable();
            $table->tinyInteger('car_type');
            $table->tinyInteger('status')->nullable();
            $table->decimal('price', 10, 2)->nullable();

            $table->foreign('user_id', 'fk_orders_users')->references('id')->on('users');
            $table->foreign('fuel_type_id', 'fk_orders_fuel_types')->references('id')->on('fuel_types');
            $table->foreign('license_category_id', 'fk_orders_license_categories')->references('id')->on('license_categories');
            $table->foreign('technical_category_id', 'fk_orders_technical_categories')->references('id')->on('technical_categories');
            $table->foreign('document_type_id', 'fk_orders_document_types')->references('id')->on('document_types');
            $table->foreign('brake_type_id', 'fk_orders_brake_types')->references('id')->on('brake_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
