<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id', false, true);
            $table->bigInteger('payment_method_id', false, true);
            $table->enum('type', ['outcome', 'income'])->nullable();
            $table->decimal('amount', 10, 2);
            $table->tinyInteger('status');
            $table->text('comment')->nullable();
            $table->timestamp('created_at')->nullable();
            //$table->timestamps();

            $table->foreign('user_id', 'fk_payments_users')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
