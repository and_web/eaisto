<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Add2ColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('last_name', 255);
            $table->string('middle_name', 255);
            $table->bigInteger('region_id', false, true);
            $table->string('city', 255);
            $table->integer('need');
            $table->tinyInteger('status');
            $table->string('insurance_company', 255)->nullable();
            $table->string('phone', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('last_name');
            $table->dropColumn('middle_name');
            $table->dropColumn('region_id');
            $table->dropColumn('city');
            $table->dropColumn('need');
            $table->dropColumn('status');
            $table->dropColumn('insurance_company');
            $table->dropColumn('phone');
        });
    }
}
