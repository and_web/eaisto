<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incomes', function (Blueprint $table) {
            $table->id()->first();
            $table->bigInteger('user_id', false, true)->after('id');
            $table->text('comment')->after('payment_method_id');
            $table->timestamp('date')->useCurrent()->after('comment');
            $table->boolean('status')->default(0)->after('date');

            $table->foreign('user_id', 'fk_incomes_users')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incomes', function (Blueprint $table) {
            $table->dropForeign('fk_incomes_users');

            $table->dropColumn('comment');
            $table->dropColumn('date');
            $table->dropColumn('id');
            $table->dropColumn('user_id');
            $table->dropColumn('status');
        });
    }
}
