<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsInPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->enum('type', ['outcome', 'income'])->nullable(false)->change();
            $table->dropColumn('payment_method_id');
            $table->dropColumn('comment');
            $table->boolean('status')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->enum('type', ['outcome', 'income'])->nullable(true)->change();
            $table->bigInteger('payment_method_id', false, true);
            $table->text('comment')->nullable();
            $table->tinyInteger('status')->change();
        });
    }
}
