<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropPaymentDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('payment_documents');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('payment_documents', function (Blueprint $table) {
            $table->bigInteger('payment_id', false, true)->primary();
            $table->bigInteger('file_id', false, true);
            $table->timestamps();

            $table->foreign('payment_id', 'fk_payment_file_payments')->references('id')->on('payments');
            $table->foreign('file_id', 'fk_payment_file_documents')->references('id')->on('documents');
        });
    }
}
