<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiagnosticCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnostic_cards', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id', false, true);
            $table->bigInteger('station_id', false, true);
            $table->bigInteger('number', false, true);
            $table->timestamp('valid_from')->useCurrent();
            $table->timestamp('valid_to')->useCurrent();
            $table->timestamps();

            $table->foreign('order_id', 'diagnostic_cards_orders')
                ->references('id')
                ->on('orders');
            $table->foreign('station_id', 'diagnostic_cards_stations')
                ->references('id')
                ->on('stations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnostic_cards');
    }
}
