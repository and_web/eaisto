<?php


namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!DB::table('document_types')->exists()) {
            $documentTypes = $this->getData();
            foreach ($documentTypes as $id => $name) {
                DB::table('document_types')->insert([
                    'name' => $name,
                ]);
            }
        }
    }
    private function getData()
    {
        return [
            '1' => 'Свидетельство регистрации транспортного средства',
            '2' => 'Паспорт транспортного средства',
            '3' => 'Электронный паспорт транспортного средства',
        ];
    }
}
