<?php

namespace Database\Seeders;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getData() as $item) {
            $this->addItem($item);
        }
    }

    protected function addItem($data)
    {
        $children = $data['children'] ?? null;
        unset($data['children']);
        $id = DB::table('admin_menu')->insertGetId($data);
        $uri = $data['uri'];
        if (isset($children)) {
            unset($data['uri']);
            foreach ($children as $child) {
                $child['parent_id'] = $id;
                $child['uri'] = $uri . '/' . $child['uri'];
                $this->addItem($child);
            }
        }

    }

    protected function getData()
    {
        return [
            [
                'title' => 'Агенты',
                'icon' => 'fa-users',
                'uri' => 'users',
            ],
            [
                'title' => 'Заказы',
                'icon' => 'fa-shopping-cart',
                'uri' => 'orders',
            ],
            [
                'title' => 'Оплата',
                'icon' => 'fa-bitcoin',
                'uri' => 'incomes',
            ],
            [
                'title' => 'Настройки',
                'icon' => 'fa-cogs',
                'uri' => 'settings',
                'children' => [
                    [
                        'title' => 'Станции',
                        'icon' => 'fa-automobile',
                        'uri' => 'stations',
                    ],
                    [
                        'title' => 'Цены',
                        'icon' => 'fa-money',
                        'uri' => 'prices',
                    ],
                    [
                        'title' => 'Опции',
                        'icon' => 'fa-bars',
                        'uri' => 'options',
                    ],
                    [
                        'title' => 'Способы оплаты',
                        'icon' => 'fa-bank',
                        'uri' => 'payment-methods',
                    ]
                ]
            ]
        ];
    }
}
