<?php


namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrakeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!DB::table('brake_types')->exists()) {
            $brakeTypes = $this->getData();
            foreach ($brakeTypes as $id => $name) {
                DB::table('brake_types')->insert([
                    'name' => $name,
                ]);
            }
        }
    }
    private function getData()
    {
        return [
            '1' => 'Механический',
            '2' => 'Гидравлический',
            '3' => 'Пневматический',
            '4' => 'Комбинированный',
            '5' => 'Без тормозной системы',
        ];
    }
}
