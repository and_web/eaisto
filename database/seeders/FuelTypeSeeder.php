<?php


namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FuelTypeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!DB::table('fuel_types')->exists()) {
            $fuelTypes = $this->getData();
            foreach ($fuelTypes as $id => $name) {
                \DB::table('fuel_types')->insert([
                    'name' => $name,
                ]);
            }
        }
    }
    private function getData()
    {
        return [
            '1' => 'Бензин',
            '2' => 'Дизель',
            '3' => 'Электрическая энергия',
            '4' => 'Сжатый газ',
            '5' => 'Сжиженный газ',
            '6' => 'Сжиженный углеводородный газ',
            '7' => 'Водород',
            '8' => 'Без топлива',
        ];
    }
}
