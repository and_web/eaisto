<?php


namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TechnicalCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!DB::table('technical_categories')->exists()) {
            $technicalCategories = $this->getData();
            foreach ($technicalCategories as $id => $name) {
                DB::table('technical_categories')->insert([
                    'name' => $name,
                ]);
            }
        }
    }
    private function getData()
    {
        return [
            '1' => 'L3',
            '2' => 'L4',
            '3' => 'L5',
            '4' => 'M1',
            '5' => 'M2',
            '6' => 'M3',
            '7' => 'N1',
            '8' => 'N2',
            '9' => 'N3',
            '10' => 'O2',
            '11' => 'O3',
            '12' => 'O4',
        ];

    }
}
