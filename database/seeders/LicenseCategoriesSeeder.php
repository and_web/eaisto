<?php


namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LicenseCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!DB::table('license_categories')->exists()) {
            $licenseCategories = $this->getData();
            foreach ($licenseCategories as $name => $description) {
                DB::table('license_categories')->insert([
                    'name' => $name, 'description' => $description
                ]);
            }
        }
    }
    private function getData()
    {
        return [
            'A' => 'мотоциклы',
            'B' => 'легковые автомобили',
            'C' => 'грузовые автомобили',
            'D' => 'автобусы',
            'E' => 'прицепы',
        ];
    }
}
