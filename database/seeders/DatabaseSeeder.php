<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminMenuSeeder::class,
            BrakeTypeSeeder::class,
            DocumentTypeSeeder::class,
            FuelTypeSeeder::class,
            LicenseCategoriesSeeder::class,
            OptionSeeder::class,
            RegionsSeeder::class,
            TechnicalCategoriesSeeder::class,
            PricesSeeder::class,
        ]);
    }
}
