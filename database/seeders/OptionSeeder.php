<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getData() as $key => $value) {
            if (!DB::table('options')->where('key', $key)->exists()) {
                DB::table('options')->insert([
                    'key' => $key,
                    'value' => $value,
                ]);
            }
        }
    }

    public function getData()
    {
        return [
            'site_enabled' => 1,
            'registration_enabled' => 1,
            'order_enabled' => 1,
            'auto_generation' => 1,
        ];
    }
}
