<?php


namespace Database\Seeders;


use Illuminate\Database\Seeder;

class PricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prices = $this->getData();
        foreach ($prices as $license_category_id => $price) {
            \DB::table('prices')->insert([
                'license_category_id' => $license_category_id,
                'value' => $price,
            ]);
        }
    }
    private function getData()
    {
        return [
            '1' => '800',
            '2' => '1000',
            '3' => '1200',
            '4' => '1500',
            '5' => '2000',
        ];
    }
}
