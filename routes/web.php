<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PricesController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserSupportController;
use App\Http\Controllers\CheckController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__.'/auth.php';

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('check', [CheckController::class, 'index'])->name('check.index');
Route::post('check/search', [CheckController::class, 'search'])->name('check.search');

Route::middleware(['auth', 'verified'])->group(function(){

    Route::prefix('account')->group(function(){
        Route::get('/show', []);
    });

    // Orders
    Route::prefix('order')->group(function () {
        Route::get('/', [OrderController::class, 'index'])->name('order.index');
        Route::get('/create', [OrderController::class, 'create'])->name('order.create');
        Route::get('/edit/{id}', [OrderController::class, 'edit'])->name('order.edit');
        Route::post('/store', [OrderController::class, 'store'])->name('order.store');
        Route::post('/update/{id}', [OrderController::class, 'update'])->name('order.update');
        Route::get('/pay/{id}', [OrderController::class, 'pay'])->name('order.pay');
        Route::post('/register', [OrderController::class, 'register'])->name('order.register');
        Route::get('/receipt/{id}', [OrderController::class, 'receipt'])->name('order.receipt');
    });

    // Payments
    Route::prefix('payment')->group(function () {
        Route::get('/create', [PaymentController::class, 'create'])->name('payment.create');
        Route::post('/store', [PaymentController::class, 'store'])->name('payment.store');
        Route::get('/', [PaymentController::class, 'index'])->name('payment.index');
    });

    // Web
    Route::get('prices', [PricesController::class, 'index'])->name('prices.index');
    Route::get('support', [UserSupportController::class, 'index'])->name('support.index');

    Route::prefix('user')->group(function () {
        Route::get('/show/{id}', [UserController::class, 'show'])->name('user.show');
        Route::get('/edit/{id}', [UserController::class, 'edit'])->name('user.edit');
        Route::post('/update/{id}', [UserController::class, 'update'])->name('user.update');
    });

});

Route::get('card', function(){
    return view('generator.template');
});
