@extends('layouts.main')

@section('content')

    <div class="container block-info">
        <div class="row mb_100">
            <div class="col-sm-12">
                <h1>Цены на ДК</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12"></div>
            <div class="col-md-6 col-sm-12 table-price">

                <table class="table table-price-title">
                    <tbody>
                    <tr>
                        <th>Категория</th>
                        <th>Стоимость 1 ДК</th>
                    </tr>
                    </tbody>
                </table>

                @foreach($prices as $price)
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>{{ $price->licenseCategory->name }}</td>
                            <th scope="row">{{ $price->value }} руб.</th>
                        </tr>
                        </tbody>
                    </table>
                @endforeach
            </div>
            <div class="col-md-3 col-sm-12"></div>
        </div>

    </div>

    {{--<div class="container block-info">
        <div class="row mb_100">
            <div class="col-sm-12">
                <h1>Пополнить баланс</h1>
            </div>
        </div>
        <div class="row mb_50">
            <div class="col-md-3 col-sm-12"></div>
            <div class="col-md-6 col-sm-12 table-balance">

                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>Агент №</td>
                        <th scope="row">1234567678</th>
                    </tr>
                    </tbody>
                </table>

                <div class="input-group input-group-lg input-group-balance">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="balance">Сумма пополнения</span>
                    </div>
                    <input name="balance" id="balance" type="text" class="form-control" placeholder="Введите сумму">
                </div>

            </div>
            <div class="col-md-3 col-sm-12"></div>
        </div>

        <div class="row form">
            <div class="col-md-4 d-none d-sm-none d-md-block"></div>

            <div class="form-group col-md-4 col-sm-12 d-flex justify-content-center">
                <button type="submit" class="btn btn-primary btn-sm" id="submit">
                    Пополнить
                </button>
            </div>
            <div class="col-md-4 col-sm-12"></div>

        </div>

    </div>--}}

    {{--<div class="container block-info">
        <div class="row mb_100">
            <div class="col-sm-12">
                <h1>Пополнить баланс</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-money-tab" data-toggle="pill" href="#v-pills-money"
                       role="tab" aria-controls="v-pills-money" aria-selected="true">
                        <img src="assets/img/money.png">
                    </a>
                    <a class="nav-link" id="v-pills-qiwi-tab" data-toggle="pill" href="#v-pills-qiwi" role="tab"
                       aria-controls="v-pills-qiwi" aria-selected="false">
                        <img src="assets/img/qiwi.png">
                    </a>
                    <a class="nav-link" id="v-pills-visa-tab" data-toggle="pill" href="#v-pills-visa" role="tab"
                       aria-controls="v-pills-visa" aria-selected="false">
                        <img src="assets/img/visa.png">
                    </a>
                </div>
            </div>
            <div class="col-sm-12 col-lg-9">
                <div class="tab-content mb_50" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-money" role="tabpanel"
                         aria-labelledby="v-pills-money-tab">
                        <div class="container block-requisites">
                            <h2 class="text-center mb_20">Реквизиты ЮMoney (ex. Yandex)</h2>
                            <div class="input-group input-group-lg input-group-balance mb_30">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="balance">№ кошелька</span>
                                </div>
                                <input name="balance" id="balance" type="text" class="form-control"
                                       placeholder="Введите номер">
                            </div>
                            <p>Инструкция по оплате через Сбербанк с 0% комиссией <a href="/">youtube.com</a></p>
                            <p class="mb_30"><span class="red">Важно!</span> Подтвердите оплату, указав время (пл Мск.)
                                и дату оплаты.</p>
                            <div class="form-group col-sm-12 d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary btn-sm" id="submit">
                                    Подтвердить
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-qiwi" role="tabpanel" aria-labelledby="v-pills-qiwi-tab">
                        <div class="container block-requisites">
                            <h2 class="text-center mb_20">Реквизиты QIWI кошелька</h2>
                            <div class="input-group input-group-lg input-group-balance mb_30">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="balance">№ кошелька киви</span>
                                </div>
                                <input name="balance" id="balance" type="text" class="form-control"
                                       placeholder="Введите номер">
                            </div>
                            <p>Инструкция по оплате через Сбербанк с 0% комиссией <a href="/">youtube.com</a></p>
                            <p class="mb_30"><span class="red">Важно!</span> Подтвердите оплату, указав время (пл Мск.)
                                и дату оплаты.</p>
                            <div class="form-group col-sm-12 d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary btn-sm" id="submit">
                                    Подтвердить
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-visa" role="tabpanel" aria-labelledby="v-pills-visa-tab">
                        <div class="container block-requisites">
                            <h2 class="text-center mb_20">Реквизиты карты</h2>
                            <div class="input-group input-group-lg input-group-balance mb_30">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="balance">№ карты</span>
                                </div>
                                <input name="balance" id="balance" type="text" class="form-control"
                                       placeholder="Введите номер">
                            </div>
                            <p>Инструкция по оплате через Сбербанк с 0% комиссией <a href="/">youtube.com</a></p>
                            <p class="mb_30"><span class="red">Важно!</span> Подтвердите оплату, указав время (пл Мск.)
                                и дату оплаты.</p>
                            <div class="form-group col-sm-12 d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary btn-sm" id="submit">
                                    Подтвердить
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-3">

            </div>
            <div class="col-sm-12 col-lg-9">
                <p class="attention"><span class="red">Внимание:</span> Пожалуйста, уточняйте каждый раз актуальные
                    реквизиты на данной странице. Платежи не будут зачислены, если вы переведете деньги на не
                    действительные реквизиты.</p>
            </div>
        </div>

    </div>--}}
@endsection
