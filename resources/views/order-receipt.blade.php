@extends('layouts.main')

@section('content')
    <div class="container block-info">
        <div class="container block-info">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <h1>Личный кабинет <span class="last-word">агента</span></h1>
                </div>
                <div class="col-md-4 col-sm-12 block-btn">
                    <a href="{{ route('order.create') }}" type="button" class="btn btn-primary btn-lg">Создать новую ДК</a>
                </div>
            </div>
        </div>

        <div class="container block_step">
            <div class="row">
                <x-step step="1" svg="/assets/img/step1.svg" text="Оформление ДК"></x-step>
                <x-step step="2" svg="/assets/img/step2.svg" text="Оплата"></x-step>
                <x-step step="3" svg="/assets/img/step3.svg" text="Регистрация и получение" :active="true"></x-step>
            </div>
        </div>

        <hr class="mb_100">

        <div class="row block-receipt">
            <div class="block-receipt-img">
                <div class="img-ok"></div>
            </div>
            <div class="col-sm-12 text-center">
                <h2>Регистрация ДК прошла успешно!</h2>
            </div>
            <div class="col-sm-12 col-lg-3"></div>
            <div class="col-sm-12 col-lg-6 table-receipt">

                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Присвоен №</td>
                            <th scope="row">793535733567</th>
                        </tr>
                    </tbody>
                </table>

                <div class="d-lg-none btn-block-check mb_20"><button type="button" class="btn btn-primary btn-lg btn-check">Проверить номер в ЕАИСТО</button></div>

                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Марка и категория</td>
                            <th scope="row">{{ $order->brand }} {{ $order->model }} - {{ $order->licenseCategory->name }}
                        </th>
                    </tbody>
                </table>

                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>Гос. регистрационный №</td>
                        <th scope="row">{{ $order->numberplate }}</th>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>Лицо, предоставившее ТС</td>
                        <th scope="row">{{ $order->first_name }} {{ $order->last_name }}</th>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="d-none d-lg-block col-lg-3  btn-block-check"><button type="button" class="btn btn-primary btn-lg btn-check">Проверить номер в ЕАИСТО</button></div>
        </div>

        <div class="row save-button mb_100">
            <div class="col-sm-12 col-lg-3 d-none d-sm-none d-md-block"></div>
            <div class="col-sm-12 col-lg-3 block-receipt-btn mb_20" style="padding-left: 8px;">
                <button type="button" class="btn btn-primary btn-lg draft-button">Перейти в список ДК</button>
            </div>
            <div class="col-sm-12 col-lg-3 block-receipt-btn" style="padding-left: 8px;">
                <button type="button" class="btn btn-primary btn-lg save-button">
                    <img src="/assets/img/download.png" alt="" style="vertical-align: middle; margin-right: 5px;">
                    Скачать
                </button>
            </div>
            <div class="col-sm-12 col-lg-3 d-none d-sm-none d-md-block"></div>
        </div>

    </div>

@endsection
