@props(['name', 'entity' => 'order'])

<input id="{{ $name }}" type="text" name="{{ $name }}" {!! $attributes->merge(['class' => 'form-control']) !!}
       value="{{ old($name) ?? $$entity->{$name} ?? '' }}">
