<div class="col-sm-12 col-md-4 block_step__item {{ $class }}">
    <div class="block_step__img">
        {!! $svg !!}
    </div>
    <div class="block_step__caption"><p>Шаг {{ $step }}</p></div>
    <div class="block_step__info"><p>{{ $text }}</p></div>
</div>


