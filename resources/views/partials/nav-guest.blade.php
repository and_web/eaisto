<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        @include('partials.logo')
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample07">
            <div class="navbar-block-menu flex-grow-1">
                <ul class="navbar-nav justify-content-end">

                    <li class="nav-item">
                        <a class="nav-link" href="/check">Проверка ДК</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">О сервисе</a>
                    </li>
                </ul>
            </div>
            <div class="navbar-block-lk">
                <a href="{{ route('login') }}" class="btn btn-primary btn-lg">Вход/Регистрация</a>
            </div>
        </div>
    </div>
</nav
