<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        @include('partials.logo')
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07"
                aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample07">
            <div class="navbar-block-menu flex-grow-1">
                <ul class="navbar-nav justify-content-end">

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('order.create') }}">Создать ДК</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('order.index') }}">Список ДК</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/check">Проверка ДК</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('prices.index') }}">Цены</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('payment.create') }}">Балланс - {{ auth()->user()->balance }} руб.</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('support.index') }}">Техподдержка</a>
                    </li>

                </ul>
            </div>
            <div class="navbar-block-lk flex-grow-1">
                <ul class="navbar-nav justify-content-end">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown07" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Привет, {{ auth()->user()->first_name }}</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown07">
                            <a class="dropdown-item" href="#">ID: {{ auth()->id() }}</a>
                            <a class="dropdown-item" href="#">Статус: {{ auth()->user()->getStatus() }}</a>
                            <a class="dropdown-item" href="{{ route('payment.index') }}">История платежей</a>
                            <a class="dropdown-item" href="{{ route('user.show', ['id' => auth()->id()]) }}">Настройки аккаунта</a>
                            <a class="dropdown-item" href="{{ route('logout') }}">Выйти</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
