@extends('layouts.main')

@section('content')
    <!--div class="container-fluid block-slide">
        <div class="container main-title">
            <p>Оформляйте техосмотр по<br /> новым правилам</p><span class="">от 01 марта 2021 года</span>
        </div>
        <div class="block-img"></div>
    </div-->


    <div class="wrapper">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-md-6 col-lg-4 main-title">
                    <div class="d-flex date-head">
                        <p>Оформляйте техосмотр по<br /> новым правилам</p>
                    </div>
                    <span class="date-head">от 01 марта 2021 года</span>
                    @guest
                        <div class="btn-reg-log">
                            <a href="/register" class="btn btn-light" type="button">Регистрация</a>
                            <a href="/login" class="btn btn-primary-home" type="button">Вход</a>
                        </div>
                    @endguest
                </div>
                <div class="col-sm-12 col-md-6 col-lg-8">
                    <img class="woman-working" src="assets/img/woman-working-office.png">
                </div>
            </div>
        </div>
    </div>

    <div class="container block-home">
        <div class="row mb_100">
            <div class="col-sm-12">
                <h1>Система по оформлению техосмотра<br /> в РФ по новым правилам</h1>
            </div>
        </div>

        <div class="container block_advantage">
            <div class="row">
                <div class="col-sm-12 col-md-3 block_advantage__item">
                    <div class="block_advantage__img_1"></div>
                    <div class="block_advantage__info"><p>Актуальная цена категории "B" 1000 руб.</p></div>
                </div>
                <div class="col-sm-12 col-md-3 block_step__item">
                    <div class="block_advantage__img_2"></div>
                    <div class="block_advantage__info"><p>Способы оплаты пополнение баланса без %</p></div>
                </div>
                <div class="col-sm-12 col-md-3 block_step__item">
                    <div class="block_advantage__img_3"></div>
                    <div class="block_advantage__info"><p>Срок регистрации 10-15 минут</p></div>
                </div>

                <div class="col-sm-12 col-md-3 block_step__item">
                    <div class="block_advantage__img_4"></div>
                    <div class="block_advantage__info"><p>Доступные регионы 75 из 85</p><p><a href="#region_list">Посмотреть список доступных регионов</a></p></div>
                </div>
            </div>
        </div>

    </div>

    <div id="region_list" class="container">

        <table class="table table-bordered table-order-list hide" style="font-weight: 700; font-size: 20px;">
            <thead>
                <tr style="font-weight: 500;">
                    <td>Код</td>
                    <td>Наименование</td>
                    <td>Кол-во доступных станций</td>
                </tr>
                @foreach($regions as $region)
                    <tr>
                        <th>{{ $region->code }}</th>
                        <th>{{ $region->name }}</th>
                        <th>{{ $region->stations->count() }}</th>
                    </tr>
                @endforeach
            </thead>
        </table>

        <table class="table table-bordered table-order-list" style="font-weight: 700; font-size: 20px;">
            <thead>
            <tr style="font-weight: 500;">
                <td>Код</td>
                <td>Наименование</td>
                <td>Кол-во доступных станций</td>
            </tr>

            @foreach($regions->random(7) as $region)
                <tr>
                    <th>{{ $region->code }}</th>
                    <th>{{ $region->name }}</th>
                    <th>{{ $region->stations->count() }}</th>
                </tr>
            @endforeach

            </thead>
        </table>

        <div class="d-flex justify-content-center my-5">
            <button type="button" class="btn btn-primary btn-lg show_region_list">Посмотреть весь список</button>
        </div>

    </div>

@endsection
