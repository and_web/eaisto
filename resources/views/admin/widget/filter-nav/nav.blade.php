<ul class="nav nav-tabs">
        @foreach($links as $link)
        <li class="nav-item {{ $link->isActive() ? 'active' : '' }}">
            <a class="nav-link" href="{{ $link->getHref() }}">
                {{ $link->getText() }}
            </a>
        </li>
        @endforeach
</ul>

