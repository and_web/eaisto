@extends('layouts.main')

@section('content')
    <div class="container block-info">
        <div class="row mb_100">
            <div class="col-md-8 col-sm-12">
                <h1>История платежей</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <table class="table">
                    <tr>
                        <th>Номер</th>
                        <th>Дата</th>
                        <th>Операция</th>
                        <th>Сумма</th>
                        <th>Остаток</th>
                    </tr>
                    @foreach($payments as $payment)
                        <tr>
                            <td>{{ $payment->id }}</td>
                            <td>{{ $payment->created_at }}</td>
                            <td>
                                @if ($payment->income)
                                    Пополнение счета через {{ $payment->income->paymentMethod->name }}
                                @elseif ($payment->outcome)
                                    Оплата за ДК {{ $payment->outcome->order->brand }}
                                    <br>
                                    {{ $payment->outcome->order->numberplate }} | {{ $payment->outcome->order->last_name }}
                                @endif
                            </td>
                            <td>
                                {{ ($payment->income ? '+' : '') . $payment->amount }}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

    </div>
@endsection
