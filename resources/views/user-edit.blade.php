@extends('layouts.main')

@section('content')
    {{--Форма редактирования--}}
    <div class="container block-info">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <h1>Редактирование аккаунта</h1>
            </div>
            <div class="col-md-4 col-sm-12">

            </div>
        </div>
    </div>

    <div class="container mb_100">
        <form method="post" action="{{ route('user.update', ['id' => $user->id]) }}">
            @csrf()
            <div class="row mb_30">
                <div class="col-lg-6">
                    <div class="">
                        <label for="id">ID</label>
                        <input id="id" class="form-control" value="{{ $user->id }}" disabled>
                    </div>
                    <div class="">
                        <label for="last_name">Фамилия</label>
                        <input name="last_name" id="last_name" class="form-control" value="{{ old('last_name') ?? $user->last_name }}">
                    </div>
                    <div class="">
                        <label for="first_name">Имя</label>
                        <input name="first_name" id="first_name" class="form-control" value="{{ old('first_name') ?? $user->first_name }}">
                    </div>
                    <div class="">
                        <label for="middle_name">Отчество</label>
                        <input name="middle_name" id="middle_name" class="form-control" value="{{ old('middle_name') ?? $user->middle_name }}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="">
                        <label for="email">Логин</label>
                        <input id="email" class="form-control" value="{{ $user->email }}" disabled>
                    </div>
                    <div class="" style="margin-bottom: 1rem;">
                        <label for="region_id">Регион</label>
                        <select name="region_id" id="region_id" class="form-control dropdown-select2">
                            <option value="">Выберите регион</option>
                            @foreach($regions as $region)
                                <option value="{{ $region->id }}" {{ $region->id == $user->region->id ? 'selected' : '' }}>
                                    {{ $region->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="" style="margin-bottom: 1rem;">
                        <label for="city">Город</label>
                        <select class="form-control dadata" name="city" id="city">
                            <option value="">Начните вводить город</option>
                            <option value="{{ $user->city }}" selected>{{ $user->city }}</option>
                        </select>
                    </div>
                    <div class="">
                        <label for="created_at">Дата регистрации</label>
                        <input id="created_at" class="form-control" value="{{ $user->created_at->format('d.m.Y') }}" disabled>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <button type="submit" class="btn btn-primary">
                        Сохранить
                    </button>
                </div>
            </div>
        </form>
    </div>

@endsection
