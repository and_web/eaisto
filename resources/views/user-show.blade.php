@extends('layouts.main')

@section('content')
    {{--Таблица данных пользователя--}}
    <div class="container block-info">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <h1>Настройки аккаунта</h1>
            </div>
            <div class="col-md-4 col-sm-12">

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row mb_30">
            <div class="col-lg-6 table-price">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>ID</td>
                            <th scope="row">{{ $user->id }}</th>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Фамилия</td>
                            <th scope="row">{{ $user->last_name }}</th>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Имя</td>
                            <th scope="row">{{ $user->first_name }}</th>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Отчество</td>
                            <th scope="row">{{ $user->middle_name }}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-6 table-price">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Логин</td>
                            <th scope="row">{{ $user->email }}</th>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Регион</td>
                            <th scope="row">{{ $user->region->name }}</th>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Город</td>
                            <th scope="row">{{ $user->city }}</th>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Дата регистрации</td>
                            <th scope="row">{{ $user->created_at->format('d.m.Y | H:i:s') }}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <a href="{{ route('user.edit', ['id' => $user->id]) }}" type="button" class="btn btn-primary">
                    Редактировать
                </a>
            </div>
        </div>
    </div>

@endsection
