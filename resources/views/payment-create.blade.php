@extends('layouts.main')

@section('content')
    <div class="container block-info">
        <div class="row mb_100">
            <div class="col-sm-12">
                <h1>Пополнить баланс</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-3 payment-pills-tab">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    @foreach($paymentMethods as $key => $method)
                        <a class="nav-link {{ $key === 0 ? 'active' : '' }}" id="v-pills-{{ $method->id }}-tab"
                                data-toggle="pill" href="#v-pills-{{ $method->id }}"
                           role="tab" aria-controls="v-pills-{{ $method->id }}" aria-selected="true">
                            <img src="{{ $method->logo }}">
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="col-sm-12 col-lg-9">
                <div class="tab-content mb_50" id="v-pills-tabContent">
                    @foreach($paymentMethods as $key => $method)
                        <div class="tab-pane fade {{ $key === 0 ? 'show active' : '' }}" id="v-pills-{{ $method->id }}"
                                role="tabpanel" aria-labelledby="v-pills-money-tab">
                            <div class="container block-requisites">
                                <h2 class="text-center mb_20">Реквизиты {{ $method->name }}</h6>
                                <div class="input-group input-group-lg input-group-balance mb_30">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="balance">{{ $method->label }}</span>
                                    </div>
                                    <!--div class="input-group-prepend">
                                        <span class="input-group-text" id="balance">{{ $method->value }}</span>
                                    </div-->
                                    <input name="balance" id="balance" type="text" class="form-control"
                                           placeholder="Введите номер">
                                </div>
                                <p>Инструкция по оплате через Сбербанк с 0% комиссией
                                    <a href="{{ $method->description }}" target="_blank">youtube.com</a>
                                </p>
                                    <p class="mb_30"><span class="red">Важно!</span> Подтвердите оплату, указав время (пл Мск.) и дату оплаты.</p>
                                <div class="form-group col-sm-12 d-flex justify-content-center">
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                            data-target="#payment-modal" data-payment-method-id="{{ $method->id }}">
                                        Подтвердить
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-3">
            </div>
            <div class="col-sm-12 col-lg-9">
                <p class="attention"><span class="red">Внимание:</span> Пожалуйста, уточняйте каждый раз актуальные
                    реквизиты на данной странице. Платежи не будут зачислены, если вы переведете деньги на не
                    действительные реквизиты.</p>
            </div>
        </div>

    </div>

    <div class="modal fade" id="payment-modal">
        <div class="modal-dialog modal-dialog-centered bd-example-modal-lg modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title text-center">
                        <h2>Подтверждение оплаты</h2>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <form method="post" action="{{ route('payment.store') }}" id="form" enctype="multipart/form-data">
                            @csrf()
                            <div class="row">
                                <div class="col-md-1 form-group"></div>
                                <div class="col-md-3 form-group">
                                    <input type="text" class="form-control" name="amount" id="amount" placeholder="Сумма"
                                           value="{{ old('amount') }}">
                                </div>
                                <div class="col-md-4 form-group">
                                    <input type="datetime-local" class="form-control" name="date" id="date"
                                           placeholder="Время оплаты" value="{{ old('date') }}">
                                </div>
                                <div class="col-md-3 form-group">
                                    <div class="input-group mb-3">
                                        <select class="custom-select" id="payment_method_id" name="payment_method_id">
                                            <option value="">Куда платили</option>
                                            @foreach($paymentMethods as $method)
                                                <option value="{{ $method->id }}">{{ $method->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1 form-group"></div>
                                <div class="col-md-10 form-group">
                                    <div class="">
                                        <textarea class="form-control" placeholder="Комментарий" name="comment"
                                                  id="comment" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1 form-group"></div>
                                <div class="col-md-10 form-group block-download">
                                    <p>Загрузите скан</p>
                                    <div class="fl_upld">
                                        <label>
                                            <input id="fl_inp" type="file" name="payment_receipt"
                                                   accept="image/*,image/jpeg">
                                            Выберите файл
                                        </label>
                                        <div id="fl_nm">Файл не выбран</div>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary btn-lg">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

@endsection
