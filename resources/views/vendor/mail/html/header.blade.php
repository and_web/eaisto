<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="https://eaisto.ru/assets/img/logo4-2.jpg" class="logo" alt="EAISTO Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
