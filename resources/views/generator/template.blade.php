<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        * {
            margin: 0;
            padding: 0;
            letter-spacing: -0.5px;
        }

        body {
            font-family: "DejaVu Serif";
            font-size: 18px;
            padding: 30px 30px;
        }

        h1 {
            font-size: 22px;
        }

        h2 {
            font-size: 18px;
        }

        table {
            border-collapse: collapse;
        }

        td {
            border: 1px solid black;
        }

        .fs-18 {
            font-size: 18px;
        }

        .fs-16 {
            font-size: 16px;
        }

        .fs-14 {
            font-size: 14px;
        }

        .fs-13 {
            font-size: 13px;
        }

        .fs-12 {
            font-size: 12px;
        }

        .fs-10 {
            font-size: 10px;
        }
        .fs-11 {
            font-size: 11px;
        }
        .fs-9 {
            font-size: 9px;
        }

        .fs-8 {
            font-size: 8px;
        }

        .text-center {
            text-align: center;
        }

        .b-0 {
            border: 0 !important;
        }
        .b-1 {
            border: 1px solid black;
        }

        .br-0 {
            border-right: 0 !important;
        }

        .frame {
            width: 20px;
            height: 20px;
            border: 1px solid black;
            padding-left: 4px;
            padding-right: 4px;
            padding-bottom: 2px;
            text-align: center;
        }

        .row {
            width: 100%;
        }

        .p-10 {
            padding: 10px;
        }

        .px-5 {
            padding-left: 5px;
            padding-right: 5px;
        }

        .px-10 {
            padding-left: 10px;
            padding-right: 10px;
        }

        .py-3 {
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .py-4 {
            padding-top: 4px;
            padding-bottom: 4px;
        }

        .py-5 {
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .pl-10 {
            padding-left: 5px;
        }
        .pl-10 {
            padding-left: 10px;
        }
        .pl-20 {
            padding-left: 20px;
        }
        .pl-100 {
            padding-left: 100px;
        }
        .mx-5 {
            margin-left: 5px;
            margin-right: 5px;
        }
        .mx-10 {
            margin-left: 10px;
            margin-right: 10px;
        }
        .my-5 {
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .my-10 {
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .mb-5 {
            margin-bottom: 5px;
        }

        .mb-10 {
            margin-bottom: 10px;
        }

        .mb-20 {
            margin-bottom: 20px;
        }

        .w-2 {
            width: 2%;
        }

        .w-3 {
            width: 3%;
        }

        .w-5 {
            width: 5%;
        }

        .w-10 {
            width: 10%;
        }
        .w-11 {
            width: 11%;
        }
        .w-12-5 {
            width: 12.5%;
        }
        .w-15 {
            width: 15%;
        }
        .w-20 {
            width: 20%;
        }

        .w-25 {
            width: 25%;
        }

        .w-28 {
            width: 28%;
        }

        .w-30 {
            width: 30%;
        }
        .w-33 {
            width: 33%;
        }
        .w-40 {
            width: 40%;
        }

        .w-50 {
            width: 50%;
        }

        .w-60 {
            width: 60%;
        }

        .w-100 {
            width: 100%;
        }

        .bold {
            font-weight: bold;
        }

        .h-14 {
            height: 14px;
        }

        .lh-24 {
            line-height: 24px;
        }
        .page-break {
            page-break-after: always;
        }
        .image {
            background-size: contain;
            background-repeat: no-repeat;
            display: inline-block;
            width: 150px;
            height: 75px;
        }
        .image.failed {
            background-image: url('http://l.eaisto/assets/img/cancel.png')
        }
        .image-text {
            width: 150px;
            height: 58px;
            border: 1px solid #000000;
            padding-top: 15px
        }
        .inline-block {
            display: inline-block;
        }
    </style>
</head>
<body>
<h1 class="text-center">Диагностическая карта</h1>
<h2 class="text-center mb-10">Certificate of periodic technical inspection</h2>
<table class="row mb-20">
    <tbody>
    <tr>
        <td class="p-10 w-60">
            <div class="mb-5">Регистрационный номер</div>
            <table>
                <tbody>
                <tr>
                @foreach(str_split($number) as $letter)
                        <td class="frame">{{ $letter }}</td>
                @endforeach
                    {{--<td class="frame">0</td>
                    <td class="frame">8</td>
                    <td class="frame">5</td>
                    <td class="frame">5</td>
                    <td class="frame">8</td>
                    <td class="frame">1</td>
                    <td class="frame">0</td>
                    <td class="frame">2</td>
                    <td class="frame">2</td>
                    <td class="frame">1</td>
                    <td class="frame">0</td>
                    <td class="frame">0</td>
                    <td class="frame">1</td>
                    <td class="frame">0</td>
                    <td class="frame">0</td>--}}
                </tr>
                </tbody>
            </table>
        </td>
        <td class="p-10 w-40">
            <div class="mb-5">Срок действия до</div>
            <table>
                <tbody>
                <tr>
                    @foreach(str_split($endDate) as $letter)
                        @if($letter !== '.' && $letter !== '/' && $letter !== '-')
                            <td class="frame">{{ $letter }}</td>
                        @endif
                    @endforeach
                    {{--<td class="frame">2</td>
                    <td class="frame">8</td>
                    <td class="frame">0</td>
                    <td class="frame">4</td>
                    <td class="frame">2</td>
                    <td class="frame">0</td>
                    <td class="frame">2</td>
                    <td class="frame">1</td>--}}
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<table class="row fs-13 mb-20">
    <tbody>
    <tr>
        <td class="px-10 py-4 bold">
            Оператор технического осмотра:
        </td>
        <td class="px-10 py-4" colspan="3">
            {{ $station->oto }}
        </td>
    </tr>
    <tr>
        <td class="px-10 py-4 bold">
            Пункт технического осмотра
            <br>
            (передвижная диагностическая линия):
        </td>
        <td class="px-10 py-4" colspan="3">
            {{ $station->pto }}
        </td>
    </tr>
    <tr>
        <td class="px-10 py-4 w-30 bold">
            Первичная проверка:
        </td>
        <td class="px-10 py-4 w-20">
            X
        </td>
        <td class="px-10 py-4 w-20 bold">
            Повторная проверка:
        </td>
        <td class="px-10 py-4 w-30"></td>
    </tr>
    <tr>
        <td class="px-10 py-4 bold">
            Регистрационный знак ТС:
        </td>
        <td class="px-10 py-4">
            {{ $order->numberplate }}
        </td>
        <td class="px-10 py-4 bold">
            Марка, модель ТС:
        </td>
        <td class="px-10 py-4">
            {{ $order->brand }} {{ $order->model }}
        </td>
    </tr>
    <tr>
        <td class="px-10 py-4 bold">
            VIN:
        </td>
        <td class="px-10 py-4">
            {{ $order->vin }}
        </td>
        <td class="px-10 py-4 bold">
            Категория ТС:
        </td>
        <td class="px-10 py-4">
            {{ $order->technicalCategory->name }}
        </td>
    </tr>
    <tr>
        <td class="px-10 py-4 bold">
            Номер рамы:
        </td>
        <td class="px-10 py-4">
            {{ $order->chassis }}
        </td>
        <td class="px-10 py-4 bold">
            Год выпуска ТС:
        </td>
        <td class="px-10 py-4">
            {{ $order->year }}
        </td>
    </tr>
    <tr>
        <td class="px-10 py-4 bold">
            Номер кузова:
        </td>
        <td class="px-10 py-4" colspan="3">
            {{ $order->body }}
        </td>
    </tr>
    <tr>
        <td class="px-10 py-4" colspan="4">
            <span class="bold">СРТС или ПТС(ЭПТС) (серия, номер, выдан (оформлен) кем, когда):</span>
            <span class="pl-10">
                {{ $order->document_series ?? '' }}
                {{ $order->document_number }}
                {{ $order->document_issue_by }}
                {{ $order->document_issue_date }}
            </span>
        </td>
    </tr>
    <tr>
        <td class="px-10 py-4" colspan="4">
            <span class="bold">Тахограф или контрольное устройство (тахограф) (марка, модель, серийный номер):</span>
            <span class="pl-10">
                {{ $order->tachograph_brand ?? '' }}
                {{ $order->tachograph_model ?? ''}}
                {{ $order->tachograph_series ?? '' }}
            </span>
        </td>
    </tr>
    </tbody>
</table>
<table class="row fs-9 page-break">
    <tbody>
    <tr>
        <td class="px-5 bold text-center">
            №
        </td>
        <td class="px-5 bold" colspan="2">
            Обязательные требования безопасности, предъявляемые к
            транспортным средствам при проведении технического осмотра
        </td>
        <td class="px-5 bold text-center">
            №
        </td>
        <td class="px-5 bold" colspan="2">
            Обязательные требования безопасности, предъявляемые к
            транспортным средствам при проведении технического осмотра
        </td>
        <td class="px-5 bold text-center">
            №
        </td>
        <td class="px-5 bold" colspan="2">
            Обязательные требования безопасности, предъявляемые к
            транспортным средствам при проведении технического осмотра
        </td>
    </tr>
    <tr>
        <td class="px-5 py-5 text-center fs-14 bold" colspan="3">
            I. Тормозные системы
        </td>
        <td class="px-5 text-center">
            22
        </td>
        <td class="px-5">
            Наличие и расположение фар и сигнальных фонарей в местах, предусмотренных конструкцией
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            42
        </td>
        <td class="px-5">
            Работоспособность запоров бортов грузовой платформы и запоров горловин цистерн
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 w-3 text-center">
            1
        </td>
        <td class="px-5 w-28">
            Соответствие показателей эффективности торможения и устойчивости торможения
        </td>
        <td class="px-5 w-2 text-center">

        </td>
        <td class="px-5 w-3 text-center">
            23
        </td>
        <td class="px-5 w-28">
            Соответствие источника света в фарах, формы, цвета и размера фар. Наличие светоотражающей контурной
            маркировки, отсутствие ее повреждения и отслоения
        </td>
        <td class="px-5 w-2 text-center">

        </td>
        <td class="px-3 w-3 text-center">
            43
        </td>
        <td class="px-5">
            Работоспособность аварийного выключателя дверей и сигнала требования остановки
        </td>
        <td class="px-5 w-2 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            2
        </td>
        <td class="px-5">
            Соответствие разности тормозных сил установленным требованиям
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 fs-14 text-center bold" colspan="3">
            IV. Стеклоочистители и стеклоомыватели
        </td>
        <td class="px-3 text-center">
            44
        </td>
        <td class="px-5">
            Работоспособность аварийных выходов, приборов внутреннего освещения салона, привода управления дверями
            и сигнализации их работы
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            3
        </td>
        <td class="px-5">
            Работоспособность рабочей тормозной системы автопоездов с пневматическим тормозным приводом в режиме
            аварийного (автоматического) торможения
        </td>
        <td class="px-5 text-center">
            -
        </td>
        <td class="px-5 text-center">
            24
        </td>
        <td class="px-5">
            Наличие и работоспособность предусмотренных изготовителем транспортного средства стеклоочистителей и
            стеклоомывателей
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            45
        </td>
        <td class="px-5">
            Наличие работоспособного звукового сигнального прибора
        </td>
        <td class="px-5 text-center">

        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            4
        </td>
        <td class="px-5">
            Отсутствие утечек сжатого воздуха из колесных тормозных камер
        </td>
        <td class="px-5 text-center">
            -
        </td>
        <td class="px-5 text-center">
            25
        </td>
        <td class="px-5">
            Обеспечение стеклоомывателем подачи жидкости в зоны очистки стекла
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            46
        </td>
        <td class="px-5">
            Наличие обозначений аварийных выходов и табличек по правилам их использования.
            Обеспечение свободного доступа к аварийным выходам
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            5
        </td>
        <td class="px-5">
            Отсутствие подтеканий тормозной жидкости, нарушения герметичности трубопроводов или соединений в
            гидравлическом тормозном приводе
        </td>
        <td class="px-5 text-center">

        </td>

        <td class="px-5 fs-14 text-center bold" colspan="3">
            V. Шины и колеса
        </td>

        <td class="px-3 text-center">
            47
        </td>
        <td class="px-5">
            Наличие задних и боковых защитных устройств, соответствие их нормам
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            6
        </td>
        <td class="px-5">
            Отсутствие коррозии, грозящей потерей герметичности или разрушением
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            26
        </td>
        <td class="px-5">
            Соответствие высоты рисунка протектора шин установленным требованиям
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            48
        </td>
        <td class="px-5">
            Работоспособность автоматического замка, ручной и автоматической блокировки седельно-сцепного устройства.
            Отсутствие видимых повреждений сцепных устройств
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            7
        </td>
        <td class="px-5">
            Отсутствие механических повреждений тормозных трубопроводов
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            27
        </td>
        <td class="px-5">
            Отсутствие признаков непригодности шин к эксплуатации
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            49
        </td>
        <td class="px-5">
            Наличие работоспособных предохранительных приспособлений у одноосных прицепов (за исключением роспусков)
            и прицепов, не оборудованных рабочей тормозной системой
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            8
        </td>
        <td class="px-5">
            Отсутствие трещин остаточной деформации деталей тормозного привода
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            28
        </td>
        <td class="px-5">
            Наличие всех болтов или гаек крепления дисков и ободьев колес
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            50
        </td>
        <td class="px-5">
            Оборудование прицепов (за исключением одноосных и роспусков) исправным устройством, поддерживающим
            сцепную петлю дышла в положении, облегчающем сцепку и расцепку с тяговым автомобилем
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            9
        </td>
        <td class="px-5">
            Исправность средств сигнализации и контроля тормозных систем
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            29
        </td>
        <td class="px-5">
            Отсутствие трещин на дисках и ободьях колес
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            51
        </td>
        <td class="px-5">
            Отсутствие продольного люфта в беззазорных тягово-сцепных устройствах с тяговой вилкой для сцепленного
            с прицепом тягача
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            10
        </td>
        <td class="px-5">
            Отсутствие набухания тормозных шлангов под давлением, трещин и видимых мест перетирания
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            30
        </td>
        <td class="px-5">
            Отсутствие видимых нарушений формы и размеров крепежных отверстий в дисках колес
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            52
        </td>
        <td class="px-5">
            Обеспечение тягово-сцепными устройствами легковых автомобилей беззазорной сцепки сухарей замкового
            устройства с шаром
        </td>
        <td class="px-5 text-center">

        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            11
        </td>
        <td class="px-5">
            Расположение и длина соединительных шлангов пневматического тормозного привода автопоездов
        </td>
        <td class="px-5 text-center">
            -
        </td>
        <td class="px-5 text-center">
            31
        </td>
        <td class="px-5">
            Установка шин на транспортное средство в соответствии с установленными требованиями
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            53
        </td>
        <td class="px-5">
            Соответствие размерных характеристик сцепных устройств установленным требованиям
        </td>
        <td class="px-5 text-center">

        </td>
    </tr>
    <tr>
        <td class="px-5 text-center bold fs-14" colspan="3">
            II. Рулевое управление
        </td>
        <td class="px-5 text-center bold fs-14" colspan="3">
            VI. Двигатель и его системы
        </td>
        <td class="px-3 text-center">
            54
        </td>
        <td class="px-5">
            Оснащение транспортных средств исправными ремнями безопасности
        </td>
        <td class="px-5 text-center">

        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            12
        </td>
        <td class="px-5">
            Работоспособность усилителя рулевого управления. Плавность изменения усилия при повороте рулевого колеса
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            32
        </td>
        <td class="px-5">
            Соответствие содержания загрязняющих веществ в отработавших газах транспортных средств установленным
            требованиям
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            55
        </td>
        <td class="px-5">
            Наличие знака аварийной остановки и медицинской аптечки (медицинских аптечек)
        </td>
        <td class="px-5 text-center">

        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            13
        </td>
        <td class="px-5">
            Отсутствие самопроизвольного поворота рулевого колеса с усилителем рулевого управления от нейтрального
            положения при работающем двигателе
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            33
        </td>
        <td class="px-5">
            Отсутствие подтекания и каплепадения топлива в системе питания
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            56
        </td>
        <td class="px-5">
            Наличие не менее 2 противооткатных упоров
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            14
        </td>
        <td class="px-5">
            Отсутствие превышения предельных значений суммарного люфта в рулевом управлении
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            34
        </td>
        <td class="px-5">
            Работоспособность запорных устройств и устройств перекрытия топлива
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            57
        </td>
        <td class="px-5">
            Наличие огнетушителей, соответствующих установленным требованиям
        </td>
        <td class="px-5 text-center">

        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            15
        </td>
        <td class="px-5">
            Отсутствие повреждения и полная комплектность деталей крепления рулевой колонки и картера рулевого механизма
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            35
        </td>
        <td class="px-5">
            Соответствие системы питания газобаллонных транспортных средств, ее размещения и установки установленным
            требованиям
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            58
        </td>
        <td class="px-5">
            Надежное крепление поручней в автобусах, запасного колеса, аккумуляторной батареи, сидений, огнетушителей и
            медицинской аптечки
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center" rowspan="2">
            16
        </td>
        <td class="px-5" rowspan="2">
            Отсутствие следов остаточной деформации, трещин и других дефектов в рулевом механизме и рулевом приводе.
            Наличие и работоспособность предусмотренного изготовителем транспортного средства рулевого демпфера и (или)
            усилителя рулевого управления.
            Отсутствие подтекания рабочей жидкости в гидросистеме усилителя рулевого управления
        </td>
        <td class="px-5 text-center" rowspan="2">

        </td>
        <td class="px-5 text-center" rowspan="2">
            36
        </td>
        <td class="px-5" rowspan="2">
            Соответствие нормам уровня шума выпускной системы
        </td>
        <td class="px-5 text-center" rowspan="2">

        </td>
        <td class="px-3 text-center">
            59
        </td>
        <td class="px-5">
            Работоспособность механизмов регулировки сидений
        </td>
        <td class="px-5 text-center">

        </td>

    </tr>
    <tr>
        <td class="px-3 text-center">
            60
        </td>
        <td class="px-5">
            Наличие надколесных грязезащитных устройств, отвечающих установленным требованиям
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            17
        </td>
        <td class="px-5">
            Отсутствие устройств, ограничивающих поворот рулевого колеса, не предусмотренных конструкцией
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center bold fs-14" colspan="3">
            VII. Прочие элементы конструкции
        </td>
        <td class="px-3 text-center">
            61
        </td>
        <td class="px-5">
            Соответствие вертикальной статической нагрузки на тяговое устройство автомобиля от сцепной петли одноосного
            прицепа (прицепа-роспуска) установленным нормам
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center bold fs-14" colspan="3">
            III. Внешние световые приборы
        </td>
        <td class="px-5 text-center">
            37
        </td>
        <td class="px-5">
            Наличие зеркал заднего вида в соответствии с установленными требованиями
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            62
        </td>
        <td class="px-5">
            Работоспособность держателя запасного колеса, лебедки и механизма подъема-опускания запасного колеса
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    <tr>
        <td class="px-5 text-center" rowspan="2">
            18
        </td>
        <td class="px-5" rowspan="2">
            Соответствие устройств освещения и световой сигнализации установленным требованиям
        </td>
        <td class="px-5 text-center" rowspan="2">

        </td>
        <td class="px-5 text-center" rowspan="2">
            38
        </td>
        <td class="px-5" rowspan="2">
            Отсутствие дополнительных предметов или покрытий, ограничивающих обзорность с места водителя. Соответствие
            полосы пленки в верхней части ветрового стекла установленным требованиям
        </td>
        <td class="px-5 text-center" rowspan="2">

        </td>
        <td class="px-3 text-center">
            63
        </td>
        <td class="px-5">
            Работоспособность механизмов подъема и опускания опор и фиксаторов транспортного положения опор
        </td>
        <td class="px-5 text-center">
            -
        </td>

    </tr>
    <tr>
        <td class="px-3 text-center">
            64
        </td>
        <td class="px-5">
            Отсутствие каплепадения масел и рабочих жидкостей
        </td>
        <td class="px-5 text-center">

        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            19
        </td>
        <td class="px-5">
            Наличие рассеивателей внешних световых приборов, отсутствие их разрушения и загрязнения. Отсутствие не
            предусмотренных конструкцией светового прибора оптических элементов
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            39
        </td>
        <td class="px-5">
            Соответствие норме светопропускания ветрового стекла, передних боковых стекол и стекол передних дверей
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            65
        </td>
        <td class="px-5">
            Установка государственных регистрационных знаков в соответствии с установленными требованиями
        </td>
        <td class="px-5 text-center">

        </td>
    </tr>
    <tr>
        <td class="px-5 text-center">
            20
        </td>
        <td class="px-5">
            Работоспособность и режим работы сигналов торможения
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-5 text-center">
            40
        </td>
        <td class="px-5">
            Отсутствие трещин на ветровом стекле в зоне очистки водительского стеклоочистителя
        </td>
        <td class="px-5 text-center">

        </td>
        <td class="px-3 text-center">
            66
        </td>
        <td class="px-5">
            Работоспособность устройства или системы вызова экстренных оперативных служб
        </td>
        <td class="px-5 text-center">

        </td>
    </tr>
    <tr>
        <td class="px-5 text-center" rowspan="3">
            21
        </td>
        <td class="px-5" rowspan="3">
            Соответствие углов регулировки и силы света фар установленным требованиям
        </td>
        <td class="px-5 text-center" rowspan="3">

        </td>
        <td class="px-5 text-center" rowspan="3">
            41
        </td>
        <td class="px-5" rowspan="3">
            Работоспособность замков дверей кузова, кабины, механизмов регулировки и фиксирующих устройств сидений,
            устройства обогрева и обдува ветрового стекла, противоугонного устройства
        </td>
        <td class="px-5 text-center" rowspan="3">

        </td>
        <td class="px-3 text-center">
            67
        </td>
        <td class="px-5">
            Отсутствие изменений в конструкции транспортного средства, внесенных в нарушение установленных требований
        </td>
        <td class="px-5 text-center">

        </td>

    </tr>
    <tr>
        <td class="px-3 text-center">
            68
        </td>
        <td class="px-5">
            Соответствие транспортного средства установленным дополнительным требованиям
        </td>
        <td class="px-5 text-center">

        </td>
    </tr>
    <tr>
        <td class="px-3 text-center">
            69
        </td>
        <td class="px-5">
            Наличие работоспособного тахографа или работоспособного контрольного устройства (тахографа)
        </td>
        <td class="px-5 text-center">
            -
        </td>
    </tr>
    </tbody>
</table>
<table class="row fs-13 mb-20">
    <tbody>
    <tr>
        <td class="px-10 py-5 bold text-center" colspan="5">
            Результаты диагностирования
        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold text-center" colspan="4">
            Параметры, по которым установлено несоответствие
        </td>
        <td class="px-10 py-5 bold text-center w-20" rowspan="2">
            Пункт диагностической карты
        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold text-center w-10">
            Нижняя граница
        </td>
        <td class="px-10 py-5 bold text-center w-10">
            Результат проверки
        </td>
        <td class="px-10 py-5 bold text-center w-10">
            Верхняя граница
        </td>
        <td class="px-10 py-5 bold text-center">
            Наименование требования
        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold text-center" colspan="4">
            Невыполненные требования
        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold text-center" colspan="2">
            Предмет проверки (узел, деталь, агрегат)
        </td>
        <td class="px-10 py-5 bold text-center" colspan="2">
            Содержание невыполненного требования (с указанием нормативного источника)
        </td>
        <td class="px-10 py-5 bold h-14">

        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold text-center h-14" colspan="2"></td>
        <td class="px-10 py-5 bold text-center h-14" colspan="2"></td>
        <td class="px-10 py-5 bold h-14"></td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold text-center h-14" colspan="2"></td>
        <td class="px-10 py-5 bold text-center h-14" colspan="2"></td>
        <td class="px-10 py-5 bold h-14"></td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold text-center h-14" colspan="2"></td>
        <td class="px-10 py-5 bold text-center h-14" colspan="2"></td>
        <td class="px-10 py-5 bold h-14"></td>
    </tr>
    <tr>
        <td class="px-10 py-5 w-30 bold" colspan="5">
            Примечания:
        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 w-30 bold h-14" colspan="5">

        </td>
    </tr>
    </tbody>
</table>
<table class="row fs-13 mb-20">
    <tbody>
    <tr>
        <td class="px-10 py-5 w-30 bold text-center" colspan="4">
            Данные транспортного средства
        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold w-25">
            Масса без нагрузки:
        </td>
        <td class="px-10 py-5 w-25">
            {{ $order->net_weight }} кг
        </td>
        <td class="px-10 py-5 bold w-25">
            Разрешенная максимальная масса:
        </td>
        <td class="px-10 py-5 w-25">
            {{ $order->max_weight }} кг
        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold">
            Тип топлива:
        </td>
        <td class="px-10 py-5">
            {{ $order->fuelType->name }}
        </td>
        <td class="px-10 py-5 bold">
            Пробег ТС:
        </td>
        <td class="px-10 py-5">
            {{ $order->mileage }} км
        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold">
            Тип тормозной системы:
        </td>
        <td class="px-10 py-5">
            {{ $order->brakeType->name }}
        </td>
        <td class="px-10 py-5 bold">
            Марка шин:
        </td>
        <td class="px-10 py-5">
            {{ $order->tires }}
        </td>
    </tr>
    <tr>
        <td class="px-10 py-5" >
            <span class="bold">Сведения о газовом баллоне (газовых баллонах)</span>
            <br>
            (год выпуска, серийный номер, даты последнего и очередного освидетельствования каждого газового баллона):
        </td>
        <td class="px-10 py-5">
            {{ $order->gas_cylinder_serial_number ?? '' }}
            {{ $order->gas_cylinder_date_last_survey ?? '' }}
            {{ $order->gas_cylinder_date_next_survey ?? '' }}
            {{ $order->gas_cylinder_year_issue ?? '' }}
        </td>
        <td class="px-10 py-5">
            <span class="bold">Сведения по газобаллонному оборудованию</span><br> (номер свидетельства о проведении
            периодических испытаний газобаллонного оборудования и дата его очередного освидетельствования):
        </td>
        <td class="px-10 py-5">
            {{ $order->gas_equipment_number ?? '' }}
            {{ $order->gas_equipment_next_survey ?? '' }}
        </td>
    </tr>
    </tbody>
</table>
<table class="row fs-13 mb-20">
    <tbody>
    <tr>
        <td class="px-10 py-5 bold w-50">
            Заключение о соответствии или несоответствии транспортного средства обязательным требованиям безопасности
            транспортных средств (подтверждающее или не подтверждающее его допуск к участию в дорожном движении)
        </td>

        <td class="px-10 py-5 bold w-50 text-center" rowspan="2">
            <div class="mx-10 my-10 pl-100 image passed text-center">
                <div class="image-text">
                    Соответствует
                    <br>
                    Passed
                </div>
            </div>
            <div class="mx-10 my-10 image failed text-center">
                <div class="image-text">
                    Не соответствует
                    <br>
                    Failed
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold w-50">
            Results of the roadworthiness inspection
        </td>
    </tr>
    </tbody>
</table>
<table class="row fs-13 mb-20">
    <tbody>
    <tr>
        <td class="px-10 py-5 w-50">
            Пункты диагностической карты, требующие повторной проверки:
        </td>
        <td>

        </td>
    </tr>
    </tbody>
</table>
<table class="row fs-13 mb-20">
    <tbody>
    <tr>
        <td class="px-10 py-5 bold w-10">
            Дата:
        </td>
        <td class="p-10 w-25">
            <table>
                <tbody>
                <tr>
                    @foreach(str_split($startDate) as $letter)
                        @if($letter !== '.' && $letter !== '/' && $letter !== '-')
                            <td class="frame">{{ $letter }}</td>
                        @endif
                    @endforeach
                    {{--<td class="frame">0</td>
                    <td class="frame">9</td>
                    <td class="frame">0</td>
                    <td class="frame">4</td>
                    <td class="frame">2</td>
                    <td class="frame">0</td>
                    <td class="frame">2</td>
                    <td class="frame">1</td>--}}
                </tr>
                </tbody>
            </table>
        </td>
        <td class="px-10 py-5 bold">
            Повторный технический осмотр провести до:
        </td>
        <td class="p-10 w-25">
            <table>
                <tbody>
                <tr>
                    <td class="frame"> </td>
                    <td class="frame"> </td>
                    <td class="frame"> </td>
                    <td class="frame"> </td>
                    <td class="frame"> </td>
                    <td class="frame"> </td>
                    <td class="frame"> </td>
                    <td class="frame"> </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="px-10 py-5 bold" colspan="2">
            Ф.И.О. технического эксперта:
        </td>
        <td class="px-10 py-5" colspan="2">
            {{ $station->expert_name }}
        </td>
    </tr>
    <tr>
        <td>
            <div class="px-10 py-10 fs-14">
                <p><b>Подпись</b></p>
                <p><i>Signature</i></p>
            </div>
        </td>
        <td colspan="3">
            <table class="b-0 fs-11">
                <tbody>
                <tr>
                    <td class="b-0">
                        <div class="px-10 py-5 mx-5 my-5 inline-block" style="border: 2px solid #000; border-radius: 10px; width: auto;">
                            <div class="text-center ">ДОКУМЕНТ ПОДПИСАН</div>
                            <div class="text-center ">ЭЛЕКТРОННОЙ ПОДПИСЬЮ</div>
                            <div style="line-height: 15px;">
                                <div class="">
                                    Сертификат
                                    &nbsp;&nbsp;
                                    {{ $station->certificate }}
                                </div>
                                <div class="">
                                    Владелец
                                    &nbsp;&nbsp;
                                    {{ $station->owner }}
                                </div>
                                <div class="">
                                    Действителен с {{ $station->valid_from }} по {{ $station->valid_to }}
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
