@extends('layouts.main')

@section('content')
    <div class="container block-info">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <h1>Личный кабинет агента</h1>
            </div>
            <div class="col-md-4 col-sm-12 block-btn">
                <a href="{{ route('order.create') }}" type="button" class="btn btn-primary btn-lg">Создать новую ДК</a>
            </div>
        </div>
    </div>

    <div class="container block_step">
        <div class="row">
            <x-step step="1" svg="/assets/img/step1.svg" text="Оформление ДК" :active="true"></x-step>
            <x-step step="2" svg="/assets/img/step2.svg" text="Оплата"></x-step>
            <x-step step="2" svg="/assets/img/step3.svg" text="Регистрация и получение"></x-step>
        </div>
    </div>

    <hr>

    <form method="post" action="{{ isset($order) ? route('order.update', ['id' => $order->id]) : route('order.store') }}"
            id="form" enctype="multipart/form-data"  class="mb_50 needs-validation" novalidate>
        @csrf()
        <div class="container block-info-agent">
            <div class="row form">

                <div class="col-sm-12 form_title">
                    <h2 class="line">Лицо, предоставившее ТС для проведения ТО</h2>
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="last_name">Фамилия<span class="star">*</span></label>

                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title="Фамилия лица, предоставившего ТС для проведения ТО">?
                    </button>
                    <input id="last_name" type="text" class="form-control" name="last_name" required
                           value="{{ old('last_name') ?? $order->last_name ?? '' }}" onkeyup="changeToUpperCase(this)">
                    <div class="invalid-feedback">
                        Фамилия должна состоять не менее, чем из 2 символов
                    </div>
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="first_name">Имя<span class="star">*</span></label>
                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title="Имя лица, предоставившего ТС для проведения ТО">?
                    </button>
                    <input id="first_name" type="text" class="form-control" name="first_name"
                           value="{{ old('first_name') ?? $order->first_name ?? '' }}" onkeyup="changeToUpperCase(this)">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="middle_name">Отчество<span class="star">*</span></label>
                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title="Отчество лица, предоставившего ТС для проведения ТО">?
                    </button>
                    <input id="middle_name" type="text" class="form-control" name="middle_name"
                           value="{{ old('middle_name') ?? $order->middle_name ?? '' }}" onkeyup="changeToUpperCase(this)">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="phone">Телефон</label>
                    <input id="phone" type="tel" class="form-control" name="phone"
                           value="{{ old('phone') ?? $order->phone ?? '' }}">
                </div>

                <div class="form-group form-check col-sm-12 col-md-6 pl_25">
                    <input type="checkbox" class="form-check-input" id="legal_entity">
                    <label class="form-check-label" for="legal_entity">Собственник является юридическим лицом</label>
                </div>
                <div id="block-company" class="form-group col-sm-12 col-md-6 hide">
                    <label for="company">Организация</label>
                    <input id="company" type="text" class="form-control" name="company"
                           value="{{ old('company') ?? $order->company ?? '' }}" onkeyup="changeToUpperCase(this)">
                </div>

            </div>
        </div>

        <div class="container block-info-vehicle">
            <div class="row form mb_50">

                <div class="col-sm-12 form_title">
                    <h2 class="line">Сведения о ТС</h2>
                </div>

                <div class="col-sm-12 col-md-4 form-group">
                    <label for="vin">VIN</label>
                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title="VIN номер должен содержать 17 знаков, среди которых только буквы(A-Z|А-Я) и цифры(0-9)">?
                    </button>
                    <input id="vin" type="text" class="form-control" name="vin" maxlength="17" onkeyup="changeToUpperCase(this)"
                           value="{{ old('vin') ?? $order->vin ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-2 form-group">
                    <label for="year">Год<span class="star">*</span></label>
                    <input id="year" type="text" class="form-control" name="year"
                           maxlength="4" value="{{ old('year') ?? $order->year ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="brand">Марка<span class="star">*</span></label>
                    <input id="brand" type="text" class="form-control" name="brand"
                           value="{{ old('brand') ?? $order->brand ?? '' }}" onkeyup="changeToUpperCase(this)">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="model">Модель<span class="star">*</span></label>
                    <input id="model" type="text" class="form-control" name="model"
                           value="{{ old('model') ?? $order->model ?? '' }}" onkeyup="changeToUpperCase(this)">
                </div>

            </div>

            <div class="row mb_50">
                <div class="col-sm-12 col-md-3 form-group">
                    <label for="body">Номер кузова</label>
                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title="Если номера кузова нет — нажмите отсутствует">?
                    </button>
                    <input id="body" type="text" class="form-control" name="body"
                           value="{{ old('body') ?? $order->body ?? '' }}" onkeyup="changeToUpperCase(this)">

                    <div class="form-group form-check pl_25">
                        <input type="checkbox" class="form-check-input" id="no_body">
                        <label class="form-check-label" for="no_body">Отсутствует</label>
                    </div>
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="chassis">Шасси (рама) №</label>
                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title="Если номера шасси нет — нажмите отсутствует">?
                    </button>
                    <input id="chassis" type="text" class="form-control" name="chassis"
                           value="{{ old('chassis') ?? $order->chassis ?? '' }}" onkeyup="changeToUpperCase(this)">

                    <div class="form-group form-check pl_25">
                        <input type="checkbox" class="form-check-input" id="no_chassis">
                        <label class="form-check-label" for="no_chassis">Отсутствует</label>
                    </div>
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="mileage">Пробег ТС (км)<span class="star">*</span></label>
                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title="Округленное (целое) число">?
                    </button>
                    <input id="mileage" type="number" class="form-control" name="mileage"
                           value="{{ old('mileage') ?? $order->mileage ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="power">Мощность ТС (л.с.)<span class="star">*</span></label>
                    <input id="power" type="number" class="form-control" name="power"
                           value="{{ old('power') ?? $order->power ?? '' }}">
                </div>
            </div>

            <div class="row mb_50">
                <div class="col-sm-12 col-md-3 form-group">
                    <label for="numberplate">Гос. регистрационный знак</label>
                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title="Например: А777АА177">?
                    </button>
                    <input id="numberplate" type="text" class="form-control" name="numberplate"
                           value="{{ old('numberplate') ?? $order->numberplate ?? '' }}" onkeyup="changeToUpperCase(this)">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="license_category_id">Категория ТС (СРТС или ПТС)<span class="star">*</span></label>
                    <select name="license_category_id" id="license_category_id" class="form-control">
                        <option value="" disabled>Выберите значение</option>
                        @foreach($licenceCategories as $item)
                            <option value="{{ $item->id }}" {{ old('license_category_id') == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                    {{ old('license_category_id') == $item->id ? 'selected' : '' }}
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="technical_category_id">Категория ТС (ОКП)<span class="star">*</span></label>
                    <select name="technical_category_id" id="technical_category_id" class="form-control">
                        <option value="" disabled>Выберите значение</option>
                        @foreach($technicalCategories as $item)
                            <option value="{{ $item->id }}" {{ old('technical_category_id') == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                    {{ old('technical_category_id') == $item->id ? 'selected' : '' }}
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="tires">Марка шин<span class="star">*</span></label>
                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title="Введите марку шин на английском языке">?
                    </button>
                    <input id="tires" type="text" class="form-control" name="tires"
                           value="{{ old('tires') ?? $order->tires ?? '' }}" onkeyup="changeToUpperCase(this)">
                </div>
            </div>

            <div class="row mb_50">
                <div class="col-sm-12 col-md-3 form-group">
                    <label for="net_weight">Масса без нагрузки (кг)<span class="star">*</span></label>
                    <input id="net_weight" type="number" class="form-control" name="net_weight"
                           value="{{ old('net_weight') ?? $order->net_weight ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="max_weight">Максимальная масса<span class="star">*</span></label>
                    <input id="max_weight" type="number" class="form-control" name="max_weight"
                           value="{{ old('max_weight') ?? $order->max_weight ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="fuel_type_id">Тип топлива<span class="star">*</span></label>
                    <select name="fuel_type_id" id="fuel_type_id" class="form-control">
                        <option value="" disabled>Выберите значение</option>
                        @foreach($fuelTypes as $item)
                            <option value="{{ $item->id }}" {{ old('fuel_type_id') == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                    {{ old('fuel_type_id') == $item->id ? 'selected' : '' }}

                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="brake_type_id">Тип тормозной системы<span class="star">*</span></label>
                    <select name="brake_type_id" id="brake_type_id" class="form-control">
                        <option value="" disabled>Выберите значение</option>
                        @foreach($brakeTypes as $item)
                            <option value="{{ $item->id }}" {{ old('brake_type_id') == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                    {{ old('brake_type_id') == $item->id ? 'selected' : '' }}
                </div>
            </div>

            <div class="row mb_50">
                <div class="col-sm-12 col-md-3 form-group">
                    <label for="document_type_id">Тип документа<span class="star">*</span></label>
                    <select name="document_type_id" id="document_type_id" class="form-control">
                        <option value="" disabled>Выберите значение</option>
                        @foreach($documentTypes as $item)
                            <option value="{{ $item->id }}" {{ old('documentTypes') == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                    {{ old('documentTypes') == $item->id ? 'selected' : '' }}
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="document_series">Серия документа</label>
                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title='Обратите внимание на поле "Тип документа". В случае, если документом является ПТС, поле "Серия" не
                    может состоять только из цифр'>?
                    </button>
                    <input name="document_series" type="text" class="form-control" id="document_series"
                           value="{{ old('document_series') ?? $order->document_series ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="document_number">Номер документа<span class="star">*</span></label>
                    <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                            title='В случае, если документом является ЭПТС, поле "Номер документа" должно состоять только из 16 цифр. Иначе
                    оно должно состоять из 6 цифр'>?
                    </button>
                    <input name="document_number" type="text" class="form-control" id="document_number"
                           value="{{ old('document_number') ?? $order->document_number ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="document_issue_date">Дата выдачи документа<span class="star">*</span></label>
                    <input type="date" min="01.01.1950" max="{{ date('d.m.Y') }}" class="form-control"
                           id="document_issue_date" name="document_issue_date"
                           value="{{ old('document_issue_date') ?? (isset($order->document_issue_date) ? $order->document_issue_date->format('Y-m-d') : '') }}">
                </div>
            </div>

            <div class="row mb_20">
                <div class="col-sm-12 col-md-12 form-group">
                    <label for="document_issue_by">Кем выдан документ<span class="star">*</span></label>
                    <input name="document_issue_by" type="text" class="form-control" id="document_issue_by"
                           value="{{ old('document_issue_by') ?? $order->document_issue_by ?? '' }}">
                </div>
            </div>

            <div class="row d-flex" style="align-items: baseline;">
                <div class="form-group form-check col-sm-12 col-md-6 pl_25">
                    <input type="checkbox" class="form-check-input" id="gas_equipment">
                    <label class="form-check-label" for="gas_equipment">Установлено газобалонное оборудование</label>
                </div>
            </div>
            <div class="row mb_50 gas_equipment hide" style="align-items: baseline;">
                    <div class="col-sm-12 form_title">
                        <h2 class="line">Газобалонное оборудование</h2>
                    </div>

                    <div class="col-sm-12 col-md-3 form-group">
                        <label for="gas_equipment_number">Номер</label>

                    <input id="gas_equipment_number" type="text" class="form-control" name="gas_equipment_number"
                           value="{{ old('gas_equipment_number') ?? $order->gas_equipment_number ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="gas_equipment_next_survey">Дата очередного освидетельствования</label>
                    <input id="gas_equipment_next_survey" type="text" class="form-control" name="gas_equipment_next_survey"
                           value="{{ old('gas_equipment_next_survey') ?? $order->gas_equipment_next_survey ?? '' }}">
                </div>


                    <div class="col-sm-12 col-md-6 form-group">

                    </div>

            </div>

            <div class="row mb_50 gas_equipment hide">
                <div class="col-sm-12">
                    <h2>Газовый баллон</h2>
                </div>
            </div>
            <div class="row gas_equipment hide" style="align-items: baseline;">
                <div class="col-sm-12 col-md-3 form-group">
                    <label for="gas_cylinder_serial_number">Серийный номер</label>

                    <input id="gas_cylinder_serial_number" type="text" class="form-control" name="gas_cylinder_serial_number"
                           value="{{ old('gas_cylinder_serial_number') ?? $order->gas_cylinder_serial_number ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="gas_cylinder_date_last_survey">Дата последнего освидетельствования</label>
                    <input id="gas_cylinder_date_last_survey" type="text" class="form-control" name="gas_cylinder_date_last_survey"
                           value="{{ old('gas_cylinder_date_last_survey') ?? $order->gas_cylinder_date_last_survey ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="gas_cylinder_date_next_survey">Дата очередного освидетельствования</label>
                    <input id="gas_cylinder_date_next_survey" type="text" class="form-control" name="gas_cylinder_date_next_survey"
                           value="{{ old('gas_cylinder_date_next_survey') ?? $order->gas_cylinder_date_next_survey ?? '' }}">
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="gas_cylinder_year_issue">Год выпуска</label>
                    <input id="gas_cylinder_year_issue" type="text" class="form-control" name="gas_cylinder_year_issue"
                           value="{{ old('gas_cylinder_year_issue') ?? $order->gas_cylinder_year_issue ?? '' }}">
                </div>
            </div>
            <div class="row mb_20">
                <div class="form-group form-check col-sm-12 col-md-6 pl_25">
                    <input type="checkbox" class="form-check-input" id="foreign_citizen">
                    <label class="form-check-label" for="foreign_citizen">Собственник - иностранный гражданин</label>
                </div>
            </div>

            <div class="row mb_50">
                <div class="form-group form-check col-sm-12 col-md-12 pl_25">
                    <input name="car_type" id="car_type_regular" type="radio" class="form-check-input"
                           value="{{ \App\Models\Order::CAR_TYPE_REGULAR }}" checked>
                    <label class="form-check-label" for="car_type_regular">
                        Обычное авто
                        <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                                title='Значение поля "Срок действия" зависит от возраста ТС'>?
                        </button>
                    </label>
                </div>
                <div class="form-group form-check col-sm-12 col-md-12 pl_25">
                    <input name="car_type" id="car_type_passenger" type="radio" class="form-check-input"
                           value="{{ \App\Models\Order::CAR_TYPE_PASSENGER }}">
                    <label class="form-check-label" for="car_type_passenger">
                        Автобус или такси
                        <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                                title='Срок действия ТО полгода (6 месяцев)'>?
                        </button>
                    </label>
                </div>
                <div class="form-group form-check col-sm-12 col-md-12 pl_25">
                    <input name="car_type" id="car_type_training" type="radio" class="form-check-input"
                           value="{{ \App\Models\Order::CAR_TYPE_TRAINING }}">
                    <label class="form-check-label" for="car_type_training">
                        Учебное авто
                        <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                                title='Срок действия ТО полгода (6 месяцев)'>?
                        </button>
                    </label>
                </div>
                <div class="form-group form-check col-sm-12 col-md-12 pl_25">
                    <input name="car_type" id="car_type_truck" type="radio" class="form-check-input"
                           value="{{ \App\Models\Order::CAR_TYPE_TRUCK }}">
                    <label class="form-check-label" for="car_type_truck">
                        Грузовик
                        <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                                title='Срок действия ТО 1 год (12 месяцев)'>?
                        </button>
                    </label>
                </div>
                <div class="form-group form-check col-sm-12 col-md-12 pl_25">
                    <input name="car_type" id="car_type_dangerous" type="radio" class="form-check-input"
                           value="{{ \App\Models\Order::CAR_TYPE_DANGEROUS }}">
                    <label class="form-check-label" for="car_type_dangerous">
                        Опасный груз
                        <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                                title='Срок действия ТО полгода (6 месяцев)'>?
                        </button>
                    </label>
                </div>
            </div>

        </div>

        <hr>

        <div class="container block-photo-vehicle">
            <div class="row">
                <div class="col-sm-12 col-md-6 form_title">
                    <h2>Фото передней части ТС</h2>
                    <div class="fl_upld">
                        <label>
                            <input id="fl_inp" type="file" name="front_car_photo">Выберите файл
                        </label>
                        <div id="fl_nm">Файл не выбран</div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 form_title">
                    <h2>Фото задней части ТС</h2>
                    <div class="fl_upld">
                        <label>
                            <input id="fl_inp2" type="file" name="back_car_photo">Выберите файл
                        </label>
                        <div id="fl_nm2">Файл не выбран</div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container block-inference">
            <div class="row mb_50">
                <div class="col-sm-12 form_title">
                    <h2 class="line">Заключение о возможности/невозможности эксплуатации транспортного средства</h2>
                </div>
                <div class="col-sm-12 col-md-3 form-group">
                    <label for="verification">Проверка<span class="star">*</span>
                        <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                                title='Данное поле заполняется автоматически и не может быть изменено'>?
                        </button>
                    </label>
                    <input type="text" class="form-control" id="verification" disabled>
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="verification_type">Вид проверки<span class="star">*</span>
                        <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                                title='Данное поле заполняется автоматически и не может быть изменено'>?
                        </button>
                    </label>
                    <input type="text" class="form-control" id="verification_type" disabled>
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="verification_date">Дата проверки<span class="star">*</span>
                        <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                                title='Данное поле заполняется автоматически и не может быть изменено'>?
                        </button>
                    </label>
                    <input type="text" class="form-control" id="verification_date" disabled>
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <label for="verification_validity">Срок действия до
                        <button type="button" class="question" data-toggle="tooltip" data-placement="top"
                                title='Данное поле заполняется автоматически и не может быть изменено'>?
                        </button>
                    </label>
                    <input type="text" class="form-control" id="verification_validity" disabled>
                </div>

                <div class="col-sm-12 col-md-12 form-group">
                    <label for="verification_comment">Примечание</label>
                    <input type="text" class="form-control" id="verification_comment" disabled>
                </div>
            </div>
        </div>

        <div class="container mb_100">
            <div class="row">
                <div class="col-md-2 d-none d-sm-none d-md-block"></div>
                <div class="col-md-4 mb_20">
                    <button type="submit" name="submit" value="save" class="btn btn-primary btn-lg save-button">
                        Сохранить в ЕАИСТО
                    </button>
                </div>
                <div class="col-md-4">
                    <button type="submit" name="submit" value="draft" class="btn btn-primary btn-lg draft-button">
                        Записать как черновик
                    </button>
                </div>
                <div class="col-md-2 d-none d-sm-none d-md-block"></div>
            </div>
        </div>
    </form>

@endsection

@isset($_GET['test'])
    <script>
        window.addEventListener('load', function(){
            var testData = {
                first_name: 'Иван',
                middle_name: 'Иванович',
                last_name: 'Иванов',
                phone: '+71234567890',
                //'company' => $request->company,
                vin: '12345678901234567',
                year: 2015,
                brand: 'Ford',
                model: 'Focus',
                body: 'ABCD1234',
                chassis: 'BCED345',
                mileage: 30000,
                power: 1234,
                numberplate: 1234,
                license_category_id: 1,
                technical_category_id: 2,
                tires: 'Pirelli',
                net_weight: 1234,
                max_weight: 2345,
                fuel_type_id: 3,
                brake_type_id: 2,
                document_type_id: 1,
                document_series: 'ADSC',
                document_number: '123456',
                document_issue_date: '2020-04-12',
                document_issue_by: 'Кем-то там',
            };
            $('#form').find('input, select').each(function(index, item){
                var name = $(item).attr('name');
                if (testData[name]) {
                    $(item).val(testData[name]);
                }
            });
        });
    </script>
@endif
