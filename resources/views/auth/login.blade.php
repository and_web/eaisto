@extends('layouts.main')

@section('content')
    <div class="container block-info">
        <div class="row mb_50">
            <div class="col-sm-12">
                <h1>Вход в систему</h1>
            </div>

        </div>

        <div class="container block-info-agent">
            <form method="post" action="{{ route('login') }}"
                  class="mb_50 needs-validation @isset($errors) was-validate @endisset" novalidate>
                @csrf
                <div class="row form">

                    <div class="col-md-3 d-none d-sm-none d-md-block"></div>
                    <div class="col-md-6 col-sm-12 form-group">
                        <label for="email">Логин</label>
                        <input type="email"
                               onkeyup="var reg=/[а-яА-ЯёЁ]/g; if(reg.test(this.value)) this.value=this.value.replace(reg, '');"
                               class="no-cyrillic form-control {{ $errors->first('email', 'is-invalid') }}"
                                name="email" id="email" required value="{{ old('login') }}">
                        <div class="invalid-feedback">
                            Логин или пароль неверные
                        </div>
                        <div class="valid-feedback">
                            Ok!
                        </div>
                    </div>
                    <div class="col-md-3 d-none d-sm-none d-md-block"></div>
                </div>
                <div class="row form">
                    <div class="col-md-3 d-none d-sm-none d-md-block"></div>
                    <div class="col-md-6 col-sm-12 form-group">
                        <label for="password">Пароль</label>
                        <input type="password" class="form-control" minlength="6" name="password" id="password" required>
                        <a href="#" class="password-control"></a>
                        <div class="invalid-feedback">Введите корректный пароль</div>
                        <div class="valid-feedback">
                            Ok!
                        </div>
                    </div>
                    <div class="col-md-3 d-none d-sm-none d-md-block"></div>
                </div>
                <div class="row form">
                    <div class="col-md-3 d-none d-sm-none d-md-block"></div>
                    <div class="form-group form-check col-sm-12 col-md-6 d-flex justify-content-end mb_50">
                        <label class="form-check-label mr_30" for="exampleCheck1">
                            Я принимаю условия <a href="#">соглашения</a>
                        </label>
                        <input type="checkbox" class="form-check-input" id="exampleCheck1"
                               onchange="document.getElementById('submit').disabled = !this.checked">
                    </div>
                    <div class="col-md-3 d-none d-sm-none d-md-block"></div>
                </div>

                <div class="row form">
                    <div class="col-md-3 d-none d-sm-none d-md-block"></div>
                    <div class="form-group col-md-3 col-sm-12 d-flex">
                        <a href="/">R-captcha</a>
                    </div>
                    <div class="form-group col-md-3 col-sm-12 d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary btn-lg log-button" id="submit" disabled="">Войти</button>
                    </div>
                    <div class="col-md-3 col-sm-12"></div>

                </div>
            </form>

            <hr class="mb_50">

            <div class="row">
                <div class="col-md-3 d-none d-sm-none d-md-block"></div>
                <div class="form-group col-md-3 col-sm-12 d-flex">
                    <a href="#" data-toggle="modal" data-target="#exampleModal2">Восстановить пароль</a>
                </div>
                <div class="form-group col-md-3 col-sm-12 d-flex justify-content-end">
                    <a href="{{ route('register') }}">Регистрация</a>
                </div>
                <div class="col-md-3 col-sm-12"></div>

            </div>
        </div>

    </div>

    <!-- Modal-1 -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered bd-example-modal-lg modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-img"></div>
                    <p>Спасибо за вашу регистрацию. Пожалуйста, подтвердите ваш емейл.
                        Если письмо не пришло в течение 10 минут, то проверьте папку Спам.</p>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <!-- Modal-2 -->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered bd-example-modal-lg modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title text-center">
                        <h2>Подтверждение оплаты</h2>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-1 form-group"></div>
                            <div class="col-md-3 form-group">
                                <input type="text" class="form-control" id="amount" placeholder="Сумма"
                                       aria-describedby="loginHelp">
                            </div>
                            <div class="col-md-4 form-group">
                                <input type="text" class="form-control" id="time" placeholder="Время оплаты"
                                       aria-describedby="loginHelp">
                            </div>
                            <div class="col-md-3 form-group">
                                <div class="input-group mb-3">
                                    <select class="custom-select" id="inputGroupSelect01">
                                        <option selected>Куда платили</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1 form-group"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 form-group"></div>
                            <div class="col-md-10 form-group">
                                <div class="">
                                    <textarea class="form-control" placeholder="Комментарий"
                                              id="exampleFormControlTextarea1" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-1 form-group"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 form-group"></div>
                            <div class="col-md-10 form-group block-download">
                                <p>Загрузите скан</p>
                                <div class="fl_upld">
                                    <label>
                                        <input id="fl_inp" type="file" name="file" accept="image/*,image/jpeg">
                                        Выберите файл
                                    </label>
                                    <div id="fl_nm">Файл не выбран</div>
                                </div>
                            </div>
                            <div class="col-md-1 form-group"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 d-flex justify-content-center">
                                <button type="button" class="btn btn-primary btn-lg">Сохранить</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>





@endsection
