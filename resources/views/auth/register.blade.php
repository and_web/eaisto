@extends('layouts.main')

@section('content')
    <div class="container block-info">
        <div class="row mb_50">
            <div class="col-sm-12">
                <h1>Регистрация нового агента</h1>
            </div>

        </div>
    </div>
    <form method="post" action="{{ route('register') }}" class="mb_50 needs-validation {{ $errors->count() ? 'was-validate' : '' }}"  novalidate>
        @csrf()
        <div class="container block-info-agent mb_100">
            <div class="row form">
                <div class="col-sm-12 col-md-4 form-group">
                    <label for="last_name">Фамилия</label>
                    <input name="last_name" id="last_name" type="text" class="form-control" minlength="2" required value="{{ old('last_name') }}">
                    <div class="invalid-feedback">
                        Фамилия должна состоять не менее, чем из 2 символов
                    </div>
                    <div class="valid-feedback">
                        Ok!
                    </div>
                </div>

                <div class="col-sm-12 col-md-4 form-group">
                    <label for="first_name">Имя</label>
                    <input type="text" id="first_name" class="form-control" name="first_name" value="{{ old('firstName') }}">
                </div>

                <div class="col-sm-12 col-md-4 form-group">
                    <label for="middle_name">Отчество</label>
                    <input type="text" id="middle_name" class="form-control" name="middle_name" value="{{ old('middleName') }}">
                </div>
            </div>

            <div class="row form">

                <div class="col-sm-12 col-md-6 form-group">
                    <label for="region_id">Ваш регион</label>
                    <select name="region_id" id="region_id" class="form-control dropdown-select2">
                        <option value="">Выберите регион</option>
                        @foreach($regions as $region)
                            <option value="{{ $region->id }}">
                                {{ $region->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="col-sm-12 col-md-6 form-group">
                    <label for="city">Город регистрации</label>
                    <select class="form-control dadata" name="city" id="city">
                        <option value="">Начните вводить город</option>
                    </select>
                    {{--<input type="text" class="form-control dadata" name="city" id="city" value="{{ old('city') }}">--}}
                </div>

            </div>

            <div class="row form">

                <div class="col-sm-12 col-md-6 form-group">
                    <label for="need">Потребность ДК в месяц</label>
                    <div class="input-group mb-3">
                        <select class="custom-select" id="need" name="need">
                            <option selected value="">Выберите свой вариант</option>
                            <option value="1">1</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <!--@foreach($regions as $region)
                                <option value="{{ $region->need }}">{{ $region->need }}</option>
                            @endforeach-->
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 form-group">
                    <label for="insurance-company">Ваша страховая или платформа</label>
                    <input name="insurance_company" id="insurance-company" type="text" class="form-control">
                </div>

            </div>

            <div class="row form">
                <div class="col-sm-12 col-md-6 form-group">
                    <label for="email">Ваш E-mail</label>
                    <input name="email" id="email" type="email" class="form-control" value="{{ old('email') }}">
                </div>

                <div class="col-sm-12 col-md-6 form-group">
                    <label for="phone">Ваш телефон +7</label>
                    <input type="tel" id="phone" class="form-control" name="phone" value="{{ old('phone') }}">
                </div>

            </div>

            <div class="row form">
                <div class="col-sm-12 col-md-6 form-group">
                    <label for="password">Пароль</label>
                    <input type="password" class="form-control" name="password"  id="password"
                           placeholder="Введите пароль" autocomplete="on">
                    <a href="#" class="password-control"></a>
                </div>

                <div class="col-sm-12 col-md-6 form-group">
                    <label for="password-confirmation">Повторить пароль</label>
                    <input type="password" class="form-control" name="password_confirmation"
                           id="password-confirmation" placeholder="Введите пароль">
                    <a href="#" class="password-control"></a>
                </div>

            </div>

            <div class="row form">
                <div class="col-md-6 d-none d-sm-none d-md-block"></div>
                <div class="form-group form-check col-sm-12 col-md-6 d-flex justify-content-end mb_50">
                    <label class="form-check-label mr_30" for="agreement">
                        Я принимаю условия <a href="#">соглашения</a>
                    </label>
                    <input type="checkbox" class="form-check-input" id="agreement"
                           onchange="document.getElementById('submit').disabled = !this.checked">
                </div>
                <div class="col-md-6 d-none d-sm-none d-md-block"></div>
            </div>

            <div class="row form">
                <div class="col-md-2 d-none d-sm-none d-md-block"></div>
                <div class="form-group col-md-4 col-sm-12 d-flex">
                    <a href="/">R-captcha</a>
                </div>
                <div class="form-group col-md-4 col-sm-12 d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary btn-lg log-button" id="submit" disabled="">
                        Зарегистрироваться
                    </button>
                </div>
                <div class="col-md-2 col-sm-12"></div>

            </div>

        </div>
    </form>

@endsection
