@extends('layouts.main')

@section('content')

    <div class="container block-info block-check">
        <div class="row">
            <div class="col-sm-12 mb_100">
                <h1>Проверка ДК</h1>
            </div>
            <div class="col-sm-12">
                <p>Уважаемые пользователи! Проверка действующей диагностической карты в сервисе единой автоматизированной системы осуществляется путем скоринга данных, введенных в поле формы проверки по базе ЕАИСТО.</p>
                <p style="font-weight: 500;">Необходимо заполнить одно поле формы поиска и нажать кнопку "Проверить"</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row mb_30">
            <div class="col-lg-6 table-check">
                <h2>Проверить ДК</h2>
                <form method="post" action="{{ route('check.search') }}">
                    @csrf()
                    <input id="numberplate" name="numberplate" class="form-control" placeholder="Рег. номер А777АА777 - только заглавные русских буквы, цифры и регион">
                    <input id="vin" name="vin" class="form-control" placeholder="Введите VIN номер">
                    <input id="body" name="body" class="form-control" placeholder="Введите номер кузова">
                    <input id="chassis" name="chassis" class="form-control" placeholder="Введите номер шасси">
                    <input id="number" name="number" class="form-control" placeholder="Введите номер ДК  (ЕАИСТО - 15 или 21 знак)">
                    <div class="row">
                        <div class="col-6">
                            <a href="#">R-captcha</a>
                        </div>
                        <div class="form-group col-md-3 col-sm-12 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary btn-lg log-button" id="submit">Проверить</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-lg-6 table-check">
                <h2>Результаты поиска</h2>
                @if(isset($order))
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>Регистрационный номер</td>
                            <th scope="row">{{ $order->numberplate }}</th>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>VIN номер</td>
                            <th scope="row">{{ $order->vin }}</th>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>Номер ЕАИСТО</td>
                            <th scope="row">{{ $order->diagnosticCard->number }}</th>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>Марка</td>
                            <th scope="row">{{ $order->brand }}</th>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>Модель</td>
                            <th scope="row">{{ $order->model }}</th>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>Категория</td>
                            <th scope="row">{{ $order->licenseCategory->name }}</th>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>Дата произведения ТО</td>
                            <th scope="row">{{ $order->diagnosticCard->valid_from }}</th>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td>Действителен до</td>
                            <th scope="row">{{ $order->diagnosticCard->valid_to }}</th>
                        </tr>
                        </tbody>
                    </table>
                @else
                    <div>
                        Не найдено
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection
