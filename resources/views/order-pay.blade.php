@extends('layouts.main')

@section('content')
    <div class="container block-info">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <h1>Личный кабинет агента</h1>
            </div>
            <div class="col-md-4 col-sm-12 block-btn">
                <a href="{{ route('order.create') }}" type="button" class="btn btn-primary btn-lg">Создать новую ДК</a>
            </div>
        </div>
    </div>

    <div class="container block_step">
        <div class="row">
            <x-step step="1" svg="/assets/img/step1.svg" text="Оформление ДК"></x-step>
            <x-step step="2" svg="/assets/img/step2.svg" text="Оплата" :active="true"></x-step>
            <x-step step="3" svg="/assets/img/step3.svg" text="Регистрация и получение"></x-step>
        </div>
    </div>

    <hr>

    <div class="container block-info">

        <div class="row block-receipt">

            <div class="col-md-3 col-sm-12"></div>
            <div class="col-md-6 col-sm-12 table-balance">

                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>Заказ №</td>
                        <th scope="row">{{ $order->id }}</th>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>Категория</td>
                        <th scope="row">{{ $order->licenseCategory->name }} - {{ $order->technicalCategory->name }}
                        </th>
                    </tbody>
                </table>

                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>Марка и модель ТС</td>
                        <th scope="row">{{ $order->brand }} {{ $order->model }}</th>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>Стоимость</td>
                        <th scope="row">{{ $order->price }} руб.</th>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>На балансе</td>
                        <th scope="row">{{ auth()->user()->balance }} руб.</th>
                    </tr>
                    </tbody>
                </table>

            </div>

            <div class="row save-button mb_100">
                <div class="col-md-3 col-lg-3 d-none d-sm-none d-md-block"></div>
                <div class="col-sm-12 col-md-3 col-lg-3 block-receipt-btn">
                    <a type="button" href="{{ route('payment.create') }}" class="btn btn-primary btn-lg draft-button">Пополнить баланс</a>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-3 block-receipt-btn">
                    <form method="post" action="{{ route('order.register') }}" id="form" enctype="multipart/form-data">
                        @csrf()
                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                        <button type="submit"
                                class="btn btn-primary btn-lg save-button" {{ $user->balance < $order->price ? 'disabled' : '' }}>
                            Оплатить
                        </button>
                    </form>
                </div>
                <div class="col-md-3 col-lg-3 d-none d-sm-none d-md-block"></div>
            </div>
        </div>

    </div>
@endsection
