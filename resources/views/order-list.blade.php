@extends('layouts.main')

@section('content')

    <div class="container block-info">
        <div class="row mb_100">
            <div class="col-md-8 col-sm-12">
                <h1>Список ДК</h1>
            </div>
            <div class="col-md-4 col-sm-12 block-btn">
                <a href="{{ route('order.create') }}" type="button" class="btn btn-primary btn-lg">Создать новую ДК</a>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-12">
                <div class="order-filter">
                    <a href="{{ route('order.index') }}">Все</a>
                    <a href="{{ route('order.index', [
                        'status' => \App\Models\Order::STATUS_DONE
                    ]) }}">Зарегистрирован</a>
                    <a href="">Ожидает регистрации</a>
                    <a href="">Удалена из ЕАИСТО</a>
                    <a href="">Черновик</a>
                </div>

                <table class="table table-bordered table-order-list">
                    <thead>
                    <tr>
                        <th scope="col">Номер</th>
                        <th>Агент</th>
                        <th>Дата и время</th>
                        <th>ТС</th>
                        <th>Статус</th><th>Действие</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($orders as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $user->last_name }}</td>
                            <td>{{ $order->crated_at }}</td>
                            <td>{{ $order->brand }} {{ $order->model }} | {{ $order->numberplate }}
                                {{ $order->licenseCategory->name }}</td>
                            <td>
                                <span class="mark-status-{{ $order->status_name }}">
                                    {{ __(ucfirst($order->status_name)) }}
                                </span>
                                @if($order->status_name === \App\Models\Order::STATUS_DONE)
                                    <br>
                                    № {{ $order->diagnosticCard->number }}
                                @endif
                            </td>
                            <td>
                                @if($order->status == \App\Models\Order::STATUS_DONE)
                                    <a type="button" target="_blank" class="btn btn-primary btn-sm"
                                            href="{{ $order->diagnosticCard->pdf->url }}">
                                        Скачать
                                    </a>
                                @elseif(
                                    $order->status == \App\Models\Order::STATUS_DRAFT
                                    || $order->status == \App\Models\Order::STATUS_PENDING
                                )
                                    <a type="button" class="btn btn-primary btn-sm"
                                            href="{{ route('order.edit', ['id' => $order->id]) }}">
                                        Редактировать
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
